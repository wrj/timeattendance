#tag Window
Begin ContainerControl ctnLeaveTypeForm
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   582
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   942
   Begin TabPanel TabPanel1
      AutoDeactivate  =   True
      Bold            =   False
      Enabled         =   True
      Height          =   508
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Panels          =   ""
      Scope           =   0
      SmallTabs       =   False
      TabDefinition   =   "ประเภทการลา\rสาเหตุการลา\rยกยอด"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   20
      Underline       =   False
      Value           =   0
      Visible         =   True
      Width           =   902
      Begin Label lblLeaveCode
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   12
         TabPanelIndex   =   1
         Text            =   "รหัสประเภทการลา"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   80
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   152
      End
      Begin TextField fldLeaveCode
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   263
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   80
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   212
      End
      Begin Label lblLeaveType
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   13
         TabPanelIndex   =   1
         Text            =   "ชื่อประเภทการลา"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   114
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   152
      End
      Begin TextField fldLeaveTypeName
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   263
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   114
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   212
      End
      Begin Label lblWorkLessThanOneYear
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   14
         TabPanelIndex   =   1
         Text            =   "พนักงานทำงานไม่ครบ 1 ปี"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   148
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   152
      End
      Begin TextField fldWorkLessThanOneYear
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   263
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "0"
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   148
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   100
      End
      Begin Label lblWorkMoreThanOneYear
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   15
         TabPanelIndex   =   1
         Text            =   "พนักงานทำงานเกิน 1 ปี"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   182
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   152
      End
      Begin TextField fldWorkMoreThanOneYear
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   263
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "0"
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   182
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   100
      End
      Begin Label lblDay1
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   375
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   16
         TabPanelIndex   =   1
         Text            =   "วัน"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   148
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   30
      End
      Begin Label lblDay2
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   375
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   17
         TabPanelIndex   =   1
         Text            =   "วัน"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   182
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   30
      End
      Begin Label lblDetail
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   18
         TabPanelIndex   =   1
         Text            =   "รายละเอียด"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   213
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   152
      End
      Begin TextArea txaDetail
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   True
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   70
         HelpTag         =   ""
         HideSelection   =   True
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   263
         LimitText       =   0
         LineHeight      =   0.0
         LineSpacing     =   1.0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Multiline       =   True
         ReadOnly        =   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollbarVertical=   True
         Styled          =   True
         TabIndex        =   4
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   213
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   327
      End
      Begin Label lblRemark
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   19
         TabPanelIndex   =   1
         Text            =   "หมายเหตุ"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   295
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   152
      End
      Begin TextArea txaRemark
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   True
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   70
         HelpTag         =   ""
         HideSelection   =   True
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   263
         LimitText       =   0
         LineHeight      =   0.0
         LineSpacing     =   1.0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Multiline       =   True
         ReadOnly        =   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollbarVertical=   True
         Styled          =   True
         TabIndex        =   5
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   295
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   327
      End
      Begin Label lblNumberDay
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   20
         TabPanelIndex   =   1
         Text            =   "ลาหยุดต่อเนื่องได้กี่วัน"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   479
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   152
      End
      Begin Label lblGender
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   21
         TabPanelIndex   =   1
         Text            =   "เพศ"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   377
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   152
      End
      Begin Label lblAcceptSalary
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   22
         TabPanelIndex   =   1
         Text            =   "การรับเงินเดือน"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   411
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   152
      End
      Begin CheckBox chkCalculateHoliday
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "ไม่นับวันหยุด"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   417
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   11
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   479
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   129
      End
      Begin Label lblDay3
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   375
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   23
         TabPanelIndex   =   1
         Text            =   "วัน"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   479
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   30
      End
      Begin TextField fldNumberDay
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   263
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   10
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "0"
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   479
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   100
      End
      Begin Label lblLeaveOther
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   24
         TabPanelIndex   =   1
         Text            =   "เพิ่มเติม"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   445
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   152
      End
      Begin CheckBox chkHrOnly
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "เฉพาะ HR"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   263
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   8
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   445
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
      Begin CheckBox chkCause
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "ต้องใส่เหตุผลการลา"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   417
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   9
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   445
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   150
      End
      Begin CheckBox chkSalary
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "ไม่รับเงินเดือน"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   263
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   7
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   411
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   110
      End
      Begin RadioButton rdoFemale
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "หญิง"
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   415
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   25
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   377
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin RadioButton rdoMale
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "ชาย"
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   346
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   26
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   377
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin RadioButton rdoAll
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "ทั้งหมด"
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   263
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   27
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   377
         Underline       =   False
         Value           =   True
         Visible         =   True
         Width           =   71
      End
      Begin Listbox lstCause
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   5
         ColumnsResizable=   False
         ColumnWidths    =   "15%,*,15%,15%,15%"
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   -1
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   2
         GridLinesVertical=   2
         HasHeading      =   True
         HeadingIndex    =   -1
         Height          =   418
         HelpTag         =   ""
         Hierarchical    =   False
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         InitialValue    =   "รหัส	ชื่อสาเหตุ	ใส่เหตุผล	Upload เอกสาร	ลาเกิน  ( วัน )"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         RequiresSelection=   False
         Scope           =   0
         ScrollbarHorizontal=   True
         ScrollBarVertical=   True
         SelectionType   =   0
         TabIndex        =   0
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   90
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   862
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
      Begin PushButton btnDelete
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "ลบ"
         Default         =   False
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   822
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   58
         Underline       =   False
         Visible         =   True
         Width           =   80
      End
      Begin PushButton btnAdd
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "เพิ่ม"
         Default         =   False
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   730
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   58
         Underline       =   False
         Visible         =   True
         Width           =   80
      End
      Begin PushButton btnAddCarry
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "เพิ่ม"
         Default         =   False
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   730
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   3
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   58
         Underline       =   False
         Visible         =   True
         Width           =   80
      End
      Begin PushButton btnDeleteCarry
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "ลบ"
         Default         =   False
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   822
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   3
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   58
         Underline       =   False
         Visible         =   True
         Width           =   80
      End
      Begin Listbox lstCarry
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   3
         ColumnsResizable=   False
         ColumnWidths    =   "*,*,*"
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   -1
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   2
         GridLinesVertical=   2
         HasHeading      =   True
         HeadingIndex    =   -1
         Height          =   418
         HelpTag         =   ""
         Hierarchical    =   False
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         InitialValue    =   "อายุงานขั้นต่ำ	อายุงานสูงสุด	จำนวนวัน"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         RequiresSelection=   False
         Scope           =   0
         ScrollbarHorizontal=   True
         ScrollBarVertical=   True
         SelectionType   =   0
         TabIndex        =   2
         TabPanelIndex   =   3
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   90
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   862
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Sub BindData()
		  Self.GetValue
		  Self.SendData
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeleteData()
		  Dim lvtInstance As New Repo.WS.LeaveType
		  
		  If Self.leaveTypeId <> "" Then
		    If lvtInstance.Delete(Self.leaveTypeId) Then
		      MsgBox "ลบข้อมูลสำเร็จ"
		      LeaveTypeWindow.cLeaveTypeListTreeView.LoadLeaveType
		    Else
		      MsgBox "ลบข้อมูลไม่สำเร็จ"
		    End
		  End If
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetValue()
		  Dim leaveTypeData AS New JSONItem
		  Dim newCause AS New JSONItem
		  Dim newCarry AS New JSONItem
		  Dim causeData AS New JSONItem
		  Dim carryData AS New JSONItem
		  
		  leaveTypeData.Value("id") = Self.leaveTypeId
		  //Tab1 ประเภทการลา
		  leaveTypeData.Value("code") = Self.fldLeaveCode.Text
		  leaveTypeData.Value("name") = Self.fldLeaveTypeName.Text
		  leaveTypeData.Value("lessYear") = Self.fldWorkLessThanOneYear.Text
		  leaveTypeData.Value("moreYear") = Self.fldWorkMoreThanOneYear.Text
		  leaveTypeData.Value("detail") = Self.txaDetail.Text
		  leaveTypeData.Value("remark") = Self.txaRemark.Text
		  
		  Dim gender AS String
		  If Self.rdoMale.Value = True Then 
		    gender = "male"
		  ElseIf Self.rdoFemale.Value = True Then 
		    gender = "female"
		  ElseIf Self.rdoAll.Value = True Then
		    gender = "all"
		  End
		  leaveTypeData.Value("gender") = gender
		  leaveTypeData.Value("pay") = Self.chkSalary.Value
		  leaveTypeData.Value("hrOnly") = Self.chkHrOnly.Value
		  leaveTypeData.Value("cause") = Self.chkCause.Value
		  leaveTypeData.Value("calculateHoliday") = Self.chkCalculateHoliday.Value
		  leaveTypeData.Value("numberDay") = Self.fldNumberDay.Text
		  
		  //Tab 2 สาเหตุการลา
		  If Self.updateCause.Count > 0 Then
		    causeData.Value("update") = Self.updateCause
		  End If
		  If Self.delCause.Count > 0 Then
		    causeData.Value("delete") = Self.delCause
		  End If
		  For i As Integer = 0 to lstCause.ListCount - 1
		    If lstCause.RowTag(i) = "" Then
		      newCause.Append(lstCause.CellTag(i,0))
		    End If
		  Next
		  If newCause.Count > 0 Then
		    causeData.Value("new") = newCause
		  End If
		  leaveTypeData.Value("causeItem") = causeData
		  
		  //Tab 3 ยกยอด
		  If Self.updateCarry.Count > 0 Then
		    carryData.Value("update") = Self.updateCarry
		  End If
		  If Self.delCarry.Count > 0 Then
		    carryData.Value("delete") = Self.delCarry
		  End If
		  For i As Integer = 0 to lstCarry.ListCount - 1
		    If lstCarry.RowTag(i) = "" Then
		      newCarry.Append(lstCarry.CellTag(i,0))
		    End If
		  Next
		  If newCarry.Count > 0 Then
		    carryData.Value("new") = newCarry
		  End If
		  leaveTypeData.Value("carryItem") = carryData
		  
		  Self.formData = leaveTypeData
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewForm()
		  Dim newCause As New JSONItem
		  Dim updateCause As New JSONItem
		  Dim delCause As New JSONItem
		  Dim newCarry As New JSONItem
		  Dim updateCarry As New JSONItem
		  Dim delCarry As New JSONItem
		  Self.leaveTypeId = ""
		  Self.newCause = newCause
		  Self.updateCause = updateCause
		  Self.delCause = delCause
		  Self.lstCause.DeleteAllRows
		  Self.newCarry = newCarry
		  Self.updateCarry = updateCarry
		  Self.delCarry = delCarry
		  Self.lstCarry.DeleteAllRows
		  Self.lstCause.DeleteAllRows
		  
		  Self.chkCalculateHoliday.Value = False
		  Self.chkCause.Value = False
		  Self.chkHrOnly.Value = False
		  Self.chkSalary.Value = False
		  Self.fldLeaveCode.Text = ""
		  Self.fldLeaveTypeName.Text = ""
		  Self.fldNumberDay.Text = "0"
		  Self.fldWorkLessThanOneYear.Text = "0"
		  Self.fldWorkMoreThanOneYear.Text = "0"
		  Self.rdoAll.Value = True
		  Self.rdoFemale.Value = False
		  Self.rdoMale.Value = False
		  Self.txaDetail.Text = ""
		  Self.txaRemark.Text = ""
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendData()
		  Dim lvtInstance As New Repo.WS.LeaveType
		  
		  If Self.formData.Value("id") = "" Then
		    If lvtInstance.Save(formData) Then
		      MsgBox "เพิ่มข้อมูลสำเร็จ"
		      LeaveTypeWindow.cLeaveTypeListTreeView.LoadLeaveType
		    Else
		      MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		    End
		  Else
		    If lvtInstance.Update(formData) Then
		      MsgBox "แก้ไขข้อมูลสำเร็จ"
		      LeaveTypeWindow.cLeaveTypeListTreeView.LoadLeaveType
		    Else
		      MsgBox "แก้ไขข้อมูลไม่สำเร็จ"
		    End
		  End If
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue(leaveTypeID As String)
		  NewForm
		  If leaveTypeID <> "" Then
		    Self.leaveTypeId = leaveTypeID
		    Dim leaveTypeInstance As new Repo.WS.LeaveType
		    Self.leaveTypeId = leaveTypeID
		    
		    Dim rawData As New JSONItem
		    Dim data As New JSONItem
		    rawData = leaveTypeInstance.GetByID(leaveTypeID)
		    data = rawData.Value("data")
		    chkCalculateHoliday.Value = data.Value("calculateHoliday")
		    chkCause.Value = data.Value("cause")
		    chkHrOnly.Value = data.Value("hrOnly")
		    chkSalary.Value = data.Value("pay")
		    fldLeaveCode.Text = data.Value("code").StringValue
		    fldLeaveTypeName.Text = data.Value("name").StringValue
		    fldNumberDay.Text = data.Value("numberDay").StringValue
		    fldWorkLessThanOneYear.Text = data.Value("lessYear").StringValue
		    fldWorkMoreThanOneYear.Text = data.Value("moreYear").StringValue
		    txaDetail.Text = data.Value("detail").StringValue
		    txaRemark.Text = data.Value("remark").StringValue
		    If data.Value("gender") = "all" Then
		      rdoAll.Value = True
		      rdoFemale.Value = False
		      rdoMale.Value = False
		    ElseIf data.Value("gender") = "male" Then
		      rdoAll.Value = False
		      rdoFemale.Value = False
		      rdoMale.Value = True
		    Else
		      rdoAll.Value = False
		      rdoFemale.Value = True
		      rdoMale.Value = False
		    End If
		    Dim causeJson As New JSONItem
		    causeJson = data.Value("leaveCause")
		    lstCause.DeleteAllRows
		    For i As Integer = 0 to causeJson.Count - 1
		      Dim cItem As New JSONItem
		      cItem = causeJson.Child(i)
		      lstCause.AddRow(cItem.Value("code").StringValue)
		      lstCause.Cell(lstCause.LastIndex,1) = cItem.Value("name")
		      If cItem.Value("detail") Then
		        lstCause.Cell(lstCause.LastIndex,2) = "ใช่"
		      Else
		        lstCause.Cell(lstCause.LastIndex,2) = "ไม่ใช่"
		      End If
		      If cItem.Value("uploadFile") Then
		        lstCause.Cell(lstCause.LastIndex,3) = "ใช่"
		      Else
		        lstCause.Cell(lstCause.LastIndex,3) = "ไม่ใช่"
		      End If
		      lstCause.Cell(lstCause.LastIndex,4) = cItem.Value("numberDay")
		      lstCause.CellTag(lstCause.LastIndex,0) = cItem
		      lstCause.RowTag(lstCause.LastIndex) = cItem.Value("id")
		    Next
		    
		    Dim carryJson As New JSONItem
		    carryJson = data.Value("leaveCarry")
		    lstCarry.DeleteAllRows
		    For i As Integer = 0 to carryJson.Count - 1
		      Dim cItem As New JSONItem
		      cItem = carryJson.Child(i)
		      lstCarry.AddRow(cItem.Value("lessYear").StringValue)
		      lstCarry.Cell(lstCarry.LastIndex,1) = cItem.Value("moreYear")
		      lstCarry.Cell(lstCarry.LastIndex,2) = cItem.Value("numberDay")
		      lstCarry.CellTag(lstCarry.LastIndex,0) = cItem
		      lstCarry.RowTag(lstCarry.LastIndex) = cItem.Value("id")
		    Next
		    LeaveTypeWindow.cLeaveTypeFormView.SetValue(data)
		  End If
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		delCarry As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		delCause As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		formData As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		leaveTypeId As String
	#tag EndProperty

	#tag Property, Flags = &h0
		newCarry As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		newCause As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		updateCarry As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		updateCause As JSONItem
	#tag EndProperty


#tag EndWindowCode

#tag Events lstCause
	#tag Event
		Sub DoubleClick()
		  Dim selData As New JSONItem
		  Dim formData As New JSONItem
		  
		  selData = lstCause.CellTag(lstCause.ListIndex,0)
		  formData.Value("id") = selData.Value("id")
		  formData.Value("code") = selData.Value("code")
		  formData.Value("name") = selData.Value("name")
		  formData.Value("detail") = selData.Value("detail")
		  formData.Value("uploadFile") = selData.Value("uploadFile")
		  formData.Value("numberDay") = selData.Value("numberDay")
		  
		  Dim causeForm As New CauseFormWindow
		  causeForm.SetValue(formData)
		  causeForm.ShowModal
		  If causeForm.cancelForm <> True Then
		    Dim cData As New JSONItem
		    cData = causeForm.causeData
		    lstCause.Cell(lstCause.ListIndex,0) = cData.Value("code")
		    lstCause.Cell(lstCause.ListIndex,1) = cData.Value("name")
		    If cData.Value("detail") Then
		      lstCause.Cell(lstCause.ListIndex,2) = "ใช่"
		    Else
		      lstCause.Cell(lstCause.ListIndex,2) = "ไม่ใช่"
		    End If
		    If cData.Value("uploadFile") Then
		      lstCause.Cell(lstCause.ListIndex,3) = "ใช่"
		    Else
		      lstCause.Cell(lstCause.ListIndex,3) = "ไม่ใช่"
		    End If
		    lstCause.Cell(lstCause.ListIndex,4) = cData.Value("numberDay")
		    lstCause.CellTag(lstCause.ListIndex,0) = cData
		    lstCause.RowTag(lstCause.ListIndex) = cData.Value("id")
		    If cData.Value("id") <> "" Then
		      Dim newItem As Boolean = True
		      Dim indexArray As Integer = 0
		      For i As Integer = 0 To Self.updateCause.Count - 1
		        Dim iItem As JSONItem = Self.updateCause.Child(i)
		        If iItem.Value("id") = cData.Value("id") Then
		          newItem = false
		          indexArray = i
		          Exit
		        End If
		      Next
		      If newItem Then
		        Self.updateCause.Append(cData)
		      Else
		        Self.updateCause.Insert(indexArray,cData)
		      End If
		    End If
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnDelete
	#tag Event
		Sub Action()
		  Dim selData As String
		  selData = lstCause.RowTag(lstCause.ListIndex)
		  If selData <> "" Then
		    Dim foundItem As Boolean = False
		    Dim indexArray As Integer = 0
		    For i As Integer = 0 To Self.updateCause.Count - 1
		      Dim iItem As JSONItem = Self.updateCause.Child(i)
		      If iItem.Value("id") = selData Then
		        foundItem = True
		        indexArray = i
		        Exit
		      End If
		    Next
		    If foundItem Then
		      Self.updateCause.Remove(indexArray)
		    End If
		    Self.delCause.Append(selData)
		    lstCause.RemoveRow(lstCause.ListIndex)
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnAdd
	#tag Event
		Sub Action()
		  Dim formData As New JSONItem
		  formData.Value("id") = ""
		  formData.Value("code") = ""
		  formData.Value("name") = ""
		  formData.Value("detail") = false
		  formData.Value("uploadFile") = false
		  formData.Value("numberDay") = 0
		  
		  Dim causeForm As New CauseFormWindow
		  causeForm.SetValue(formData)
		  causeForm.ShowModal
		  If causeForm.cancelForm <> True Then
		    Dim cData As New JSONItem
		    cData = causeForm.causeData
		    lstCause.AddRow(cData.Value("code").StringValue)
		    lstCause.Cell(lstCause.LastIndex,1) = cData.Value("name")
		    If cData.Value("detail") Then
		      lstCause.Cell(lstCause.LastIndex,2) = "ใช่"
		    Else
		      lstCause.Cell(lstCause.LastIndex,2) = "ไม่ใช่"
		    End If
		    If cData.Value("uploadFile") Then
		      lstCause.Cell(lstCause.LastIndex,3) = "ใช่"
		    Else
		      lstCause.Cell(lstCause.LastIndex,3) = "ไม่ใช่"
		    End If
		    lstCause.Cell(lstCause.LastIndex,4) = cData.Value("numberDay")
		    lstCause.CellTag(lstCause.LastIndex,0) = cData
		    lstCause.RowTag(lstCause.LastIndex) = cData.Value("id")
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnAddCarry
	#tag Event
		Sub Action()
		  Dim formData As New JSONItem
		  formData.Value("id") = ""
		  formData.Value("lessYear") = "0"
		  formData.Value("moreYear") = "0"
		  formData.Value("numberDay") = "0"
		  
		  Dim carryForm As New CarryFormWindow
		  carryForm.SetValue(formData)
		  carryForm.ShowModal
		  If carryForm.cancelForm <> True Then
		    Dim cData As New JSONItem
		    cData = carryForm.carryData
		    lstCarry.AddRow(cData.Value("lessYear").StringValue)
		    lstCarry.Cell(lstCarry.LastIndex,1) = cData.Value("moreYear")
		    lstCarry.Cell(lstCarry.LastIndex,2) = cData.Value("numberDay")
		    lstCarry.CellTag(lstCarry.LastIndex,0) = cData
		    lstCarry.RowTag(lstCarry.LastIndex) = cData.Value("id")
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnDeleteCarry
	#tag Event
		Sub Action()
		  Dim selData As String
		  selData = lstCarry.RowTag(lstCarry.ListIndex)
		  If selData <> "" Then
		    Dim foundItem As Boolean = False
		    Dim indexArray As Integer = 0
		    For i As Integer = 0 To Self.updateCarry.Count - 1
		      Dim iItem As JSONItem = Self.updateCarry.Child(i)
		      If iItem.Value("id") = selData Then
		        foundItem = True
		        indexArray = i
		        Exit
		      End If
		    Next
		    If foundItem Then
		      Self.updateCarry.Remove(indexArray)
		    End If
		    Self.delCarry.Append(selData)
		    lstCarry.RemoveRow(lstCarry.ListIndex)
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lstCarry
	#tag Event
		Sub DoubleClick()
		  Dim selData As New JSONItem
		  Dim formData As New JSONItem
		  
		  selData = lstCarry.CellTag(lstCarry.ListIndex,0)
		  formData.Value("id") = selData.Value("id")
		  formData.Value("lessYear") = selData.Value("lessYear")
		  formData.Value("moreYear") = selData.Value("moreYear")
		  formData.Value("numberDay") = selData.Value("numberDay")
		  
		  
		  Dim carryForm As New CarryFormWindow
		  carryForm.SetValue(formData)
		  carryForm.ShowModal
		  If carryForm.cancelForm <> True Then
		    Dim cData As New JSONItem
		    cData = carryForm.carryData
		    lstCarry.Cell(lstCarry.ListIndex,0) = cData.Value("lessYear")
		    lstCarry.Cell(lstCarry.ListIndex,1) = cData.Value("moreYear")
		    lstCarry.Cell(lstCarry.ListIndex,2) = cData.Value("numberDay")
		    lstCarry.CellTag(lstCarry.ListIndex,0) = cData
		    lstCarry.RowTag(lstCarry.ListIndex) = cData.Value("id")
		    If cData.Value("id") <> "" Then
		      Dim newItem As Boolean = True
		      Dim indexArray As Integer = 0
		      For i As Integer = 0 To Self.updateCarry.Count - 1
		        Dim iItem As JSONItem = Self.updateCarry.Child(i)
		        If iItem.Value("id") = cData.Value("id") Then
		          newItem = false
		          indexArray = i
		          Exit
		        End If
		      Next
		      If newItem Then
		        Self.updateCarry.Append(cData)
		      Else
		        Self.updateCarry.Insert(indexArray,cData)
		      End If
		    End If
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="leaveTypeId"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

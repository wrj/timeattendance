#tag Window
Begin ContainerControl ctnAddNewLeaveDocumentForm
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cE9E9E900
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   582
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   942
   Begin Rectangle Rectangle1
      AutoDeactivate  =   True
      BorderWidth     =   1
      BottomRightColor=   &c00000000
      Enabled         =   True
      FillColor       =   &c00FFFFFF
      Height          =   485
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      Top             =   43
      TopLeftColor    =   &c00000000
      Visible         =   True
      Width           =   902
      Begin Label lblLeaveAmount
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   666
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   9
         TabPanelIndex   =   0
         Text            =   "จำนวนวันที่ลา"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   222
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   97
      End
      Begin Label lblEndDate
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   414
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   10
         TabPanelIndex   =   0
         Text            =   "ถึง"
         TextAlign       =   1
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   222
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   30
      End
      Begin Label lblStartDate
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   11
         TabPanelIndex   =   0
         Text            =   "วันที่ลา"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   222
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblLeaveDetail
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   12
         TabPanelIndex   =   0
         Text            =   "รายละเอียด"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   298
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin TextArea txaLeaveCauseDetail
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   True
         BackColor       =   &c00FFFFFF
         Bold            =   False
         Border          =   True
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   76
         HelpTag         =   ""
         HideSelection   =   True
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   211
         LimitText       =   0
         LineHeight      =   0.0
         LineSpacing     =   1.0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Multiline       =   True
         ReadOnly        =   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollbarVertical=   True
         Styled          =   True
         TabIndex        =   7
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   298
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   579
      End
      Begin Label lblleavePeriod
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   14
         TabPanelIndex   =   0
         Text            =   "ระยะเวลาการลา"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   261
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblfirstnameTH
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   15
         TabPanelIndex   =   0
         Text            =   "ชื่อพนักงาน"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   63
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblLeaveType
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   16
         TabPanelIndex   =   0
         Text            =   "ประเภทการลา"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   100
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblLastNameTH
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   515
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   17
         TabPanelIndex   =   0
         Text            =   "นามสกุล"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   63
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   63
      End
      Begin TextField fldFirstNameTH
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &c00FFFFFF
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   303
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   63
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   200
      End
      Begin TextField fldPersonnelCode
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &c00FFFFFF
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   211
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   63
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   80
      End
      Begin TextField fldLastNameTH
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &c00FFFFFF
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   590
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   63
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   200
      End
      Begin Listbox lstLeaveType
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   2
         ColumnsResizable=   False
         ColumnWidths    =   ""
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   -1
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   2
         GridLinesVertical=   2
         HasHeading      =   True
         HeadingIndex    =   -1
         Height          =   70
         HelpTag         =   ""
         Hierarchical    =   False
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         InitialValue    =   "ประเภทการลา	เหลือวันลา"
         Italic          =   False
         Left            =   211
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         RequiresSelection=   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollBarVertical=   True
         SelectionType   =   0
         TabIndex        =   22
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   100
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   579
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
      Begin CheckBox chkHalfDay
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "ลาครึ่งวัน"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   211
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   24
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   261
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
      Begin Rectangle Rectangle2
         AutoDeactivate  =   True
         BorderWidth     =   1
         BottomRightColor=   &c00FFFFFF
         Enabled         =   True
         FillColor       =   &c00FFFFFF
         Height          =   26
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Left            =   338
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   28
         TabPanelIndex   =   0
         Top             =   259
         TopLeftColor    =   &c00FFFFFF
         Visible         =   True
         Width           =   215
         Begin RadioButton rdoHalfMorning
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "กะแรก"
            Enabled         =   False
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "Rectangle2"
            Italic          =   False
            Left            =   340
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   0
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   261
            Underline       =   False
            Value           =   True
            Visible         =   True
            Width           =   100
         End
         Begin RadioButton rdoHalfAfternoon
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "กะสอง"
            Enabled         =   False
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "Rectangle2"
            Italic          =   False
            Left            =   451
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   0
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   261
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   100
         End
      End
      Begin TextField fldStartDate
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &c00FFFFFF
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   211
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   29
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   222
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   115
      End
      Begin TextField fldEndDate
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &c00FFFFFF
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   456
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   30
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   222
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   115
      End
      Begin TextArea txaRemark
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   True
         BackColor       =   &c00FFFFFF
         Bold            =   False
         Border          =   True
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   76
         HelpTag         =   ""
         HideSelection   =   True
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   211
         LimitText       =   0
         LineHeight      =   0.0
         LineSpacing     =   1.0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Multiline       =   True
         ReadOnly        =   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollbarVertical=   True
         Styled          =   True
         TabIndex        =   31
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   389
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   579
      End
      Begin Label lblRemark
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   32
         TabPanelIndex   =   0
         Text            =   "หมายเหตุ"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   391
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblNumberDayVal
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   775
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   33
         TabPanelIndex   =   0
         Text            =   ""
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   223
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin PopupMenu popLeaveCause
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         InitialValue    =   ""
         Italic          =   False
         Left            =   211
         ListIndex       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   23
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   185
         Underline       =   False
         Visible         =   True
         Width           =   292
      End
      Begin Label lblLeaveCause
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   19
         TabPanelIndex   =   0
         Text            =   "สาเหตุการลา"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   185
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin PushButton btnCalStart
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "ปฎิทิน"
         Default         =   False
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   338
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   34
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   223
         Underline       =   False
         Visible         =   True
         Width           =   64
      End
      Begin PushButton btnCalEnd
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "ปฎิทิน"
         Default         =   False
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   590
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   35
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   223
         Underline       =   False
         Visible         =   True
         Width           =   64
      End
   End
   Begin PushButton btnCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ยกเลิก"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   750
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   540
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Sub BindData()
		  Self.formData = Self.GetValue
		  Self.SendData
		  // ManagerLeaveSystemWindow.pgpLeave.Value = 0
		  // ManagerLeaveSystemWindow.cLeaveDocumentForm.SetID(Self.currentID)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CalculateDay()
		  Dim calDay AS New Repo.WS.LeaveDocument
		  Dim result As NEW JSONItem
		  Dim data AS New JSONItem
		  Dim holiday AS JSONItem = lstLeaveType.CellTag(lstLeaveType.LastIndex,0)
		  data.Value("id") = Self.personnelID
		  data.Value("startDate") = fldStartDate.Text
		  data.Value("endDate") = fldEndDate.Text
		  data.Value("holiday") = holiday.Value("calculateHoliday")
		  
		  result = calDay.CalculateLeaveDay(data)
		  
		  Self.leaveDay = result.Value("leaveDay")
		  lblNumberDayVal.Text = result.Value("leaveNumber")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeleteData()
		  Dim lvtInstance As New Repo.WS.LeaveDocument
		  
		  If Self.leaveId <> "" Then
		    If lvtInstance.Delete(Self.leaveId) Then
		      MsgBox "ลบข้อมูลสำเร็จ"
		      LeaveDocumentWindow.cLeaveDocumentTreeView.LoadLeaveList
		    Else
		      MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		    End
		  End
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetValue() As JSONItem
		  Dim leaveData AS NEW JSONItem
		  
		  leaveData.Value("organization") = Self.orgId
		  leaveData.Value("personnel") = Self.personnelID
		  leaveData.Value("leaveType") = lstLeaveType.RowTag(lstLeaveType.ListIndex)
		  leaveData.Value("startDate") = fldStartDate.Text
		  leaveData.Value("endDate") = fldEndDate.Text
		  leaveData.Value("halfDay") = chkHalfDay.Value
		  
		  If rdoHalfMorning.Value = True Then
		    leaveData.Value("halfDayType") = 0
		  ElseIf rdoHalfAfternoon.Value = True Then
		    leaveData.Value("halfDayType") = 1
		  End
		  leaveData.Value("numberDay") = Val(lblNumberDayVal.Text)
		  leaveData.Value("documentStatus") = 1
		  leaveData.Value("detail") = txaLeaveCauseDetail.Text
		  leaveData.Value("remark") = txaRemark.Text
		  If popLeaveCause.ListIndex > 0 Then
		    Dim leaveCause AS JSONItem = popLeaveCause.RowTag(popLeaveCause.ListIndex)
		    leaveData.Value("leaveCause") = leaveCause.Value("id")
		  End If
		  leaveData.Value("leaveDay") = Self.leaveDay
		  leaveData.Value("leaveDocumentID") = Self.leaveId
		  
		  
		  Return leaveData
		  //MsgBox(leaveData.ToString)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadLeaveCause(causeData AS JSONItem)
		  // Dim lvtInstance As New Repo.WS.LeaveType
		  // 
		  // Dim jsData As JSONItem
		  
		  popLeaveCause.DeleteAllRows
		  
		  // jsData = lvtInstance.GetByID(leaveTypeID)
		  // if jsData.Value("status") = true Then
		  // Dim leaveCauseList As JSONItem = jsData.Value("data")
		  //MsgBox(leaveCauseList.Child("leaveCause").ToString)
		  Dim causeD As New JSONItem
		  popLeaveCause.AddRow("สาเหตุการลา")
		  popLeaveCause.RowTag(popLeaveCause.ListCount-1) = causeD
		  
		  For i AS Integer = 0 To causeData.Count-1
		    Dim iItem As JSONItem = causeData.Child(i)
		    //MsgBox(iItem.Value("id") + " = " + iItem.Value("name"))
		    popLeaveCause.AddRow(iItem.Value("name"))
		    popLeaveCause.RowTag(popLeaveCause.ListCount-1) = iItem
		  Next
		  popLeaveCause.ListIndex = 0
		  // End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadLeaveDocumentTemplate(personnelID AS String)
		  Dim lvtInstance As New Repo.WS.LeaveDocument
		  Dim js As String
		  Dim jsData As JSONItem
		  
		  lstLeaveType.DeleteAllRows
		  jsData = lvtInstance.GetByID(personnelID)
		  
		  if jsData.Value("status") = true Then
		    Dim personnelData As JSONItem = jsData.Value("personData")
		    Self.personnelID = personnelData.Value("id")
		    fldFirstNameTH.Text =personnelData.Value("firstNameTh")
		    fldLastNameTH.Text = personnelData.Value("lastNameTh")
		    
		    
		    Dim leaveList As JSONItem = jsData.Value("leaveList")
		    For i AS Integer = 0 To leaveList.Count-1
		      Dim iItem As JSONItem = leaveList.Child(i)
		      lstLeaveType.AddRow()
		      lstLeaveType.RowTag(lstLeaveType.LastIndex) = iItem.Value("leaveTypeId")
		      lstLeaveType.CellTag(lstLeaveType.LastIndex,0) = iItem
		      lstLeaveType.Cell(lstLeaveType.LastIndex,0) = iItem.Value("name")
		      lstLeaveType.Cell(lstLeaveType.LastIndex,1) = Str(iItem.Value("netLeaveDay").IntegerValue)
		    Next
		  End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewDocument(orgID As String)
		  Dim today As New Date()
		  fldStartDate.Text = Str(today.Day)+"/"+Str(today.Month)+"/"+Str(today.Year)
		  fldEndDate.Text = ""
		  lblNumberDayVal.Text = ""
		  fldFirstNameTH.Text = ""
		  fldLastNameTH.Text = ""
		  fldPersonnelCode.Text = ""
		  lstLeaveType.DeleteAllRows
		  popLeaveCause.DeleteAllRows
		  rdoHalfMorning.Value = false
		  rdoHalfAfternoon.Value = false
		  txaLeaveCauseDetail.Text = ""
		  txaRemark.Text = ""
		  chkHalfDay.Value = false
		  Dim formData As New JSONItem
		  Self.formData = formData
		  Self.leaveDay = formData
		  Self.leaveId = ""
		  Self.orgId = orgID
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendData()
		  Dim lvtInstance As New Repo.WS.LeaveDocument
		  
		  If Self.leaveId = "" Then
		    If lvtInstance.Save(formData) Then
		      MsgBox "เพิ่มข้อมูลสำเร็จ"
		      LeaveDocumentWindow.cLeaveDocumentTreeView.LoadLeaveList
		    Else
		      MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		    End
		  Else
		    If lvtInstance.Update(formData) Then
		      MsgBox "แก้ไขข้อมูลสำเร็จ"
		      LeaveDocumentWindow.cLeaveDocumentTreeView.LoadLeaveList
		    Else
		      MsgBox "แก้ไขข้อมูลไม่สำเร็จ"
		    End
		  End
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetID(id AS String,orgID As String)
		  Dim leaveTypeInstance As new Repo.WS.LeaveDocument
		  Self.leaveId = id
		  
		  Dim data As New JSONItem
		  data = leaveTypeInstance.GetLeaveDocumentByID(id)
		  Self.formData = data
		  Self.SetValue(Self.formData)
		  Self.orgId = orgID
		  //MsgBox(Self.formData.ToString)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue(data As JSONItem)
		  If data.Value("status") = true Then
		    Dim personnelLeaveData AS JSONItem = data.Value("data")
		    LeaveDocumentWindow.cLeaveDocumentForm.SetValue(personnelLeaveData)
		    
		    
		    Dim personData AS JSONItem = personnelLeaveData.Value("personData")
		    Self.personnelID = personData.Value("id")
		    fldPersonnelCode.Text = personData.Value("code")
		    fldFirstNameTH.Text = personData.Value("firstNameTh")
		    fldLastNameTH.Text = personData.Value("lastNameTh")
		    
		    Dim leaveType AS New Repo.WS.LeaveDocument
		    Dim leaveTypeData AS New JSONItem
		    Dim leaveCauseSelectIndex As Integer = 0
		    leaveTypeData = leaveType.GetByID(personData.Value("code"))
		    
		    if leaveTypeData.Value("status") = true Then
		      Dim leaveList As JSONItem = leaveTypeData.Value("leaveList")
		      For i AS Integer = 0 To leaveList.Count-1
		        Dim iItem As JSONItem = leaveList.Child(i)
		        //List ประเภทการลา
		        If personnelLeaveData.Value("leaveTypeId") = iItem.Value("leaveTypeId") Then
		          lstLeaveType.DeleteAllRows
		          lstLeaveType.AddRow()
		          lstLeaveType.RowTag(lstLeaveType.LastIndex) = iItem.Value("leaveTypeId")
		          lstLeaveType.CellTag(lstLeaveType.LastIndex,0) = iItem
		          lstLeaveType.Cell(lstLeaveType.LastIndex,0) = iItem.Value("name")
		          lstLeaveType.Cell(lstLeaveType.LastIndex,1) = Str(iItem.Value("netLeaveDay").IntegerValue)
		          lstLeaveType.Selected(0) = True
		          //List สาเหตุการลา
		          Dim leaveCauseList As JSONItem = iItem.Value("cause")
		          //MsgBox("Leave List = " + leaveCauseList.Count.ToText)
		          popLeaveCause.DeleteAllRows
		          Dim causeD As New JSONItem
		          popLeaveCause.AddRow("สาเหตุการลา")
		          popLeaveCause.RowTag(popLeaveCause.ListCount-1) = causeD
		          For j AS Integer = 0 To leaveCauseList.Count-1
		            Dim leaveCauseData As JSONItem = leaveCauseList.Child(j)
		            popLeaveCause.AddRow(leaveCauseData.Value("name"))
		            popLeaveCause.RowTag(j) = leaveCauseData
		            If personnelLeaveData.HasName("leaveCauseId") Then
		              If(leaveCauseData.Value("id") = personnelLeaveData.Value("leaveCauseId")) Then
		                leaveCauseSelectIndex = j
		              End
		            End If
		          Next
		        End
		      Next
		    End
		    popLeaveCause.ListIndex = leaveCauseSelectIndex
		    fldStartDate.Text = personnelLeaveData.Value("startDate") 
		    fldEndDate.Text = personnelLeaveData.Value("endDate")
		    Self.CalculateDay
		    
		    If personnelLeaveData.Value("halfDay") Then
		      chkHalfDay.Value = True
		    Else
		      chkHalfDay.Value = False
		      rdoHalfMorning.Visible = False
		      rdoHalfAfternoon.Visible = False
		    End
		    
		    txaLeaveCauseDetail.Text = personnelLeaveData.Value("detail")
		    txaRemark.Text = personnelLeaveData.Value("remark")
		  End
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		formData As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private holiday As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		leaveDay As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private leaveId As String
	#tag EndProperty

	#tag Property, Flags = &h0
		numDayUpFile As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		orgId As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private personnelID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		selectCause As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		uploadFile As Boolean = false
	#tag EndProperty


#tag EndWindowCode

#tag Events fldPersonnelCode
	#tag Event
		Sub LostFocus()
		  If Me.Text <> "" Then
		    LoadLeaveDocumentTemplate(Me.Text)
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lstLeaveType
	#tag Event
		Sub Change()
		  If Me.ListIndex <> -1 Then
		    Dim iItem As JSONItem = Me.CellTag(Me.ListIndex,0)
		    Self.selectCause = iItem.Value("selectCause")
		    LoadLeaveCause(iItem.Value("cause"))
		    //MsgBox(iItem.Value("leaveTypeId").StringValue)
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkHalfDay
	#tag Event
		Sub Action()
		  If me.Value = True Then
		    rdoHalfMorning.Enabled = True
		    rdoHalfAfternoon.Enabled = True
		    
		  Else
		    rdoHalfMorning.Enabled = False
		    rdoHalfAfternoon.Enabled = False
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events fldStartDate
	#tag Event
		Sub LostFocus()
		  If fldStartDate.Text <> "" AND Me.Text <> "" AND Len(fldStartDate.Text) = 10 AND Len(fldEndDate.Text) = 10 Then
		    CalculateDay
		  Else
		    MsgBox("กรุณาใส่วันที่ให้ถูกต้อง")
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events fldEndDate
	#tag Event
		Sub LostFocus()
		  If fldStartDate.Text <> "" AND Me.Text <> "" AND Len(fldStartDate.Text) = 10 AND Len(fldEndDate.Text) = 10 Then
		    CalculateDay
		  Else
		    MsgBox("กรุณาใส่วันที่ให้ถูกต้อง")
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events popLeaveCause
	#tag Event
		Sub Change()
		  Dim selData As New JSONItem
		  selData = Me.RowTag(Me.ListIndex)
		  If selData.HasName("id") Then
		    Self.uploadFile = selData.Value("uploadFile")
		    Self.numDayUpFile = selData.Value("numberDay").IntegerValue
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnCalStart
	#tag Event
		Sub Action()
		  Dim selDay As New Date
		  If Len(fldStartDate.Text) = 10 AND Val(Mid(fldStartDate.Text,1,2)) <> 0 AND Val(Mid(fldStartDate.Text,4,2)) <> 0 AND Val(Mid(fldStartDate.Text,7,4)) <> 0 Then
		    selDay.Day = Val(Mid(fldStartDate.Text,1,2))
		    selDay.Month = Val(Mid(fldStartDate.Text,4,2))
		    selDay.Year = Val(Mid(fldStartDate.Text,7,4))
		  End If
		  Dim calWin As New CalendarWindow
		  calWin.Top = Self.Window.Top+Self.Top+Me.Top
		  calWin.Left = Self.Window.Left+Self.Left+Me.Left
		  calWin.SetDate(selDay)
		  calWin.ShowModal
		  If calWin.day <> "" Then
		    fldStartDate.Text = calWin.day+"/"+calWin.month+"/"+calWin.year
		    If fldEndDate.Text <> "" Then
		      CalculateDay
		    End
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnCalEnd
	#tag Event
		Sub Action()
		  Dim selDay As New Date
		  If Len(fldEndDate.Text) = 10 AND Val(Mid(fldEndDate.Text,1,2)) <> 0 AND Val(Mid(fldEndDate.Text,4,2)) <> 0 AND Val(Mid(fldEndDate.Text,7,4)) <> 0 Then
		    selDay.Day = Val(Mid(fldEndDate.Text,1,2))
		    selDay.Month = Val(Mid(fldEndDate.Text,4,2))
		    selDay.Year = Val(Mid(fldEndDate.Text,7,4))
		  End If
		  Dim calWin As New CalendarWindow
		  calWin.Top = Self.Window.Top+Self.Top+Me.Top
		  calWin.Left = Self.Window.Left+Self.Left+Me.Left
		  calWin.SetDate(selDay)
		  calWin.ShowModal
		  If calWin.day <> "" Then
		    fldEndDate.Text = calWin.day+"/"+calWin.month+"/"+calWin.year
		    If fldStartDate.Text <> "" Then
		      CalculateDay
		    End
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnCancel
	#tag Event
		Sub Action()
		  LeaveDocumentWindow.pgpLeave.Value = 0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="numDayUpFile"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="orgId"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="selectCause"
		Group="Behavior"
		InitialValue="false"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="uploadFile"
		Group="Behavior"
		InitialValue="false"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

#tag Window
Begin ContainerControl ctnLeaveDocumentTreeView
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   652
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   True
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   300
   Begin Rectangle Rectangle1
      AutoDeactivate  =   True
      BorderWidth     =   1
      BottomRightColor=   &c00000000
      Enabled         =   True
      FillColor       =   &cE9E9E900
      Height          =   652
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      Top             =   0
      TopLeftColor    =   &c00000000
      Visible         =   True
      Width           =   300
      Begin SearchControl SearchControl1
         AutoDeactivate  =   True
         Enabled         =   True
         HasCancelButton =   True
         HasMenu         =   False
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Left            =   8
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         PlaceHolderText =   ""
         Scope           =   0
         SendSearchStringImmediately=   False
         SendWholeSearchString=   False
         TabIndex        =   17
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         Top             =   96
         Visible         =   True
         Width           =   209
      End
      Begin Listbox lstAllLeaveDoc
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   3
         ColumnsResizable=   False
         ColumnWidths    =   "15%,*,25%"
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   25
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   2
         GridLinesVertical=   2
         HasHeading      =   True
         HeadingIndex    =   -1
         Height          =   522
         HelpTag         =   ""
         Hierarchical    =   False
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         InitialValue    =   "ลำดับ	ชื่อ - นามสกุล	วันที่ลา"
         Italic          =   False
         Left            =   0
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         RequiresSelection=   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollBarVertical=   True
         SelectionType   =   0
         TabIndex        =   18
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   130
         Underline       =   False
         UseFocusRing    =   False
         Visible         =   True
         Width           =   300
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
      Begin PopupMenu popOrg
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         InitialValue    =   ""
         Italic          =   False
         Left            =   8
         ListIndex       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Scope           =   0
         TabIndex        =   19
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   62
         Underline       =   False
         Visible         =   True
         Width           =   284
      End
      Begin PushButton btnSearch
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "ค้นหา"
         Default         =   False
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   229
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   23
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   96
         Underline       =   False
         Visible         =   True
         Width           =   51
      End
   End
   Begin Rectangle Rectangle2
      AutoDeactivate  =   True
      BorderWidth     =   1
      BottomRightColor=   &c00000000
      Enabled         =   True
      FillColor       =   &cE9E9E900
      Height          =   50
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      Top             =   0
      TopLeftColor    =   &c00000000
      Visible         =   True
      Width           =   300
   End
   Begin Label lblTitle
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   50
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      Text            =   ""
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   16.0
      TextUnit        =   0
      Top             =   0
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   284
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Sub LoadLeaveList()
		  Dim leaveDocInstance As New Repo.WS.LeaveDocument
		  Dim js As String
		  Dim jsData As JSONItem
		  
		  lstAllLeaveDoc.DeleteAllRows
		  // Dim docStatus As String = ""
		  // If cboNew.Value Then
		  // docStatus = "0"
		  // End If
		  // If cboComplete.Value Then
		  // docStatus = "1"
		  // End If
		  
		  js = leaveDocInstance.ListAll(popOrg.RowTag(popOrg.ListIndex),SearchControl1.Text,"1")
		  jsData = New JSONItem(js)
		  If jsData.Value("status") = True Then
		    Dim objData As JSONItem = jsData.Value("data")
		    For i As Integer = 0 To objData.Count - 1
		      Dim iItem As JSONItem = objData.Child(i)
		      lstAllLeaveDoc.AddRow()
		      lstAllLeaveDoc.CellTag(lstAllLeaveDoc.LastIndex,0) = iItem.Value("id")
		      lstAllLeaveDoc.Cell(lstAllLeaveDoc.LastIndex,0) = Str(i+1)
		      lstAllLeaveDoc.Cell(lstAllLeaveDoc.LastIndex,1) = iItem.Value("fullname")
		      lstAllLeaveDoc.Cell(lstAllLeaveDoc.LastIndex,2) = iItem.Value("dateCreated")
		    Next
		    If lstAllLeaveDoc.ListCount <> -1 Then
		      lstAllLeaveDoc.ListIndex = 0
		    Else
		      LeaveDocumentWindow.btnEdit.Visible = false
		      LeaveDocumentWindow.btnEdit.Enabled = false
		      LeaveDocumentWindow.btnDelete.Visible = false
		      LeaveDocumentWindow.btnDelete.Enabled = false
		      LeaveDocumentWindow.btnSave.Visible = false
		      LeaveDocumentWindow.btnSave.Enabled = false
		      LeaveDocumentWindow.btnCancel.Visible = false
		      LeaveDocumentWindow.btnCancel.Enabled = false
		      LeaveDocumentWindow.pgpLeave.Value = 0
		    End If
		  End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadOrgList()
		  Dim cpnInstance As New Repo.WS.Organize
		  Dim js As String
		  Dim jsData As JSONItem
		  
		  popOrg.DeleteAllRows
		  js = cpnInstance.ListAll()
		  jsData = New JSONItem(js)
		  If jsData.Value("status") = True Then
		    Dim objData As JSONItem = jsData.Value("data")
		    For i As Integer = 0 To objData.Count - 1
		      Dim iItem As JSONItem = objData.Child(i)
		      popOrg.AddRow(iItem.Value("nameTh"))
		      popOrg.RowTag(i) = iItem.Value("id")
		    Next
		    popOrg.ListIndex = 0
		    LoadLeaveList
		  End
		  
		End Sub
	#tag EndMethod


#tag EndWindowCode

#tag Events lstAllLeaveDoc
	#tag Event
		Sub Change()
		  If Me.ListIndex <> -1 Then
		    LeaveDocumentWindow.cAddNewLeaveDocumentForm.SetID(Me.CellTag(Me.ListIndex, 0),popOrg.RowTag(popOrg.ListIndex))
		    LeaveDocumentWindow.btnEdit.Visible = True
		    LeaveDocumentWindow.btnEdit.Enabled = True
		    LeaveDocumentWindow.btnDelete.Visible = True
		    LeaveDocumentWindow.btnDelete.Enabled = True
		    LeaveDocumentWindow.pgpLeave.Value = 0
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events popOrg
	#tag Event
		Sub Change()
		  LoadLeaveList
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSearch
	#tag Event
		Sub Action()
		  LoadLeaveList
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

#tag Window
Begin ContainerControl ctnAddNewTimeTables1
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   582
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   942
   Begin TabPanel TabPanel1
      AutoDeactivate  =   True
      Bold            =   False
      Enabled         =   True
      Height          =   474
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Panels          =   ""
      Scope           =   0
      SmallTabs       =   False
      TabDefinition   =   "ข้อมูลทั่วไป\rข้อกำหนดการหักสาย"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   54
      Underline       =   False
      Value           =   0
      Visible         =   True
      Width           =   902
      Begin Label lblCode
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   0
         TabPanelIndex   =   1
         Text            =   "รหัสกะงาน"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   123
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblName
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   1
         TabPanelIndex   =   1
         Text            =   "ชื่อกะงาน"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   157
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblTimeIn
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   2
         TabPanelIndex   =   1
         Text            =   "เวลาเข้า"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   191
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblTimeInLate
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   469
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   3
         TabPanelIndex   =   1
         Text            =   "เข้าสายได้"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   191
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblTimeOut
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   4
         TabPanelIndex   =   1
         Text            =   "เวลาออก"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   225
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblTimeOutBefore
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   469
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   5
         TabPanelIndex   =   1
         Text            =   "ออกก่อนได้"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   225
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblBreakOut
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   6
         TabPanelIndex   =   1
         Text            =   "เวลาพักเบรค"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   259
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblBreakIn
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   469
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   7
         TabPanelIndex   =   1
         Text            =   "สิ้นสุดเวลาพักเบรค"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   259
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label lblTotalHour
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   8
         TabPanelIndex   =   1
         Text            =   "รวมเวลาทำงาน"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   293
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin TextField fldCode
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   211
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   9
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   123
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   233
      End
      Begin TextField fldName
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   211
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   10
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   157
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   233
      End
      Begin TextField fldTimeIn
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   211
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   11
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "00:00"
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   191
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   233
      End
      Begin TextField fldTimeInLate
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   581
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   12
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   191
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   233
      End
      Begin TextField fldTimeOut
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   211
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   13
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "00:00"
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   225
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   233
      End
      Begin TextField fldBreakOut
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   211
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   15
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "00:00"
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   259
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   233
      End
      Begin TextField fldTotalHour
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   211
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   17
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   293
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   233
      End
      Begin TextField fldBreakIn
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   581
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   16
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "00:00"
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   259
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   233
      End
      Begin TextField fldTimeOutBefore
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   581
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   14
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   225
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   233
      End
      Begin Label Label6
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   826
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   19
         TabPanelIndex   =   1
         Text            =   "นาที"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   16.0
         TextUnit        =   0
         Top             =   191
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   41
      End
      Begin Label Label7
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   826
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   20
         TabPanelIndex   =   1
         Text            =   "นาที"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   16.0
         TextUnit        =   0
         Top             =   223
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   41
      End
      Begin GroupBox GroupBox1
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "หักสาย"
         Enabled         =   True
         Height          =   181
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   64
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   26
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   123
         Underline       =   False
         Visible         =   True
         Width           =   814
         Begin TextField fldLateTimeNumber
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   203
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   241
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   80
         End
         Begin RadioButton rdoLateCalculate
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "ไม่หักสาย"
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   93
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   2
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   160
            Underline       =   False
            Value           =   True
            Visible         =   True
            Width           =   438
         End
         Begin RadioButton rdoLateTimeCalculate
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "หักตามจำนวนครั้ง"
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   93
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   2
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   207
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   438
         End
         Begin Label lblLateDayNumber
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   295
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   3
            TabPanelIndex   =   2
            Text            =   "ครั้ง"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   241
            Transparent     =   True
            Underline       =   False
            Visible         =   True
            Width           =   40
         End
         Begin Label lblLateTimeNumber
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   111
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   4
            TabPanelIndex   =   2
            Text            =   "เข้างานสาย"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   241
            Transparent     =   True
            Underline       =   False
            Visible         =   True
            Width           =   80
         End
         Begin TextField fldLateDayNumber
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   399
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   241
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   80
         End
         Begin Label lblLateDayNumber1
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   347
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   6
            TabPanelIndex   =   2
            Text            =   "หัก"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   241
            Transparent     =   True
            Underline       =   False
            Visible         =   True
            Width           =   40
         End
         Begin Label lblLateDayNumber2
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   491
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   7
            TabPanelIndex   =   2
            Text            =   "วัน"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   241
            Transparent     =   True
            Underline       =   False
            Visible         =   True
            Width           =   40
         End
      End
      Begin GroupBox GroupBox2
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "หักออก"
         Enabled         =   True
         Height          =   181
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   64
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   27
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   316
         Underline       =   False
         Visible         =   True
         Width           =   814
         Begin Label lblBeforeDayNumber
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   295
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   0
            TabPanelIndex   =   2
            Text            =   "ครั้ง"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   433
            Transparent     =   True
            Underline       =   False
            Visible         =   True
            Width           =   40
         End
         Begin Label lblBeforeTimeNumber
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   111
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   1
            TabPanelIndex   =   2
            Text            =   "ออกก่อน"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   433
            Transparent     =   True
            Underline       =   False
            Visible         =   True
            Width           =   80
         End
         Begin RadioButton rdoBeforeTimeCalculate
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "หักตามจำนวนครั้ง"
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   84
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   2
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   399
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   447
         End
         Begin RadioButton rdoBeforeCalculate
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "ไม่หักสาย"
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   84
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   3
            TabPanelIndex   =   2
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   352
            Underline       =   False
            Value           =   True
            Visible         =   True
            Width           =   447
         End
         Begin TextField fldBeforeTimeNumber
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   203
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   435
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   80
         End
         Begin TextField fldBeforeDayNumber
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   399
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   435
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   80
         End
         Begin Label lblLateDayNumber4
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   491
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   6
            TabPanelIndex   =   2
            Text            =   "วัน"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   433
            Transparent     =   True
            Underline       =   False
            Visible         =   True
            Width           =   40
         End
         Begin Label lblLateDayNumber3
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   347
            LockBottom      =   False
            LockedInPosition=   True
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   7
            TabPanelIndex   =   2
            Text            =   "หัก"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   14.0
            TextUnit        =   0
            Top             =   433
            Transparent     =   True
            Underline       =   False
            Visible         =   True
            Width           =   40
         End
      End
      Begin Label Label13
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   99
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   21
         TabPanelIndex   =   1
         Text            =   "วันหยุด"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   327
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin CheckBox chkVacation
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   211
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   18
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   327
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Sub BindData()
		  Self.formData = Self.GetValue
		  Self.formData.Value("timeID") = Self.timeId
		  Self.SendData
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CalculateBreak(timeOut As String,timeIn As String) As Integer
		  Dim timeOutTime As new Date
		  Dim timeInTime As new Date
		  Dim timeOutArray(-1) as String
		  Dim timeInArray(-1) as String
		  
		  timeOutArray=Split(timeOut,":")
		  timeInArray=Split(timeIn,":")
		  
		  timeOutTime.Hour = Val(timeOutArray(0))
		  timeOutTime.Minute = Val(timeOutArray(1))
		  
		  timeInTime.Hour = Val(timeInArray(0))
		  timeInTime.Minute = Val(timeInArray(1))
		  
		  return timeInTime.TotalSeconds - timeOutTime.TotalSeconds
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CalculateWorkHour(timeIn As String,timeOut As String,breakTime As Integer) As Double
		  Dim timeInArray(-1) as String
		  Dim timeOutArray(-1) as String
		  Dim startTime As New Date
		  Dim endTime As New Date
		  
		  timeInArray=Split(fldTimeIn.Text,":")
		  timeOutArray=Split(fldTimeOut.Text,":")
		  
		  startTime.Hour = Val(timeInArray(0))
		  startTime.Minute = Val(timeInArray(1))
		  endTime.Hour = Val(timeOutArray(0))
		  endTime.Minute = Val(timeOutArray(1))
		  if endTime < startTime Then
		    endTime.TotalSeconds = endTime.TotalSeconds + (24.*60.*60)
		  End If
		  
		  
		  return (((endTime.TotalSeconds - startTime.TotalSeconds - breakTime)/60)/60)
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CleanValue()
		  Self.fldCode.Text = ""
		  Self.fldName.Text = ""
		  Self.fldTimeIn.Text = ""
		  Self.fldTimeInLate.Text = ""
		  Self.fldTimeOut.Text = ""
		  Self.fldTimeOutBefore.Text = ""
		  Self.fldBreakOut.Text = ""
		  Self.fldBreakIn.Text = ""
		  Self.fldTotalHour.text = ""
		  Self.chkVacation.Value = false
		  Self.rdoLateCalculate.Value = true
		  Self.rdoLateTimeCalculate.Value = false
		  Self.rdoBeforeCalculate.Value = true
		  Self.rdoBeforeTimeCalculate.Value = false
		  Self.fldLateTimeNumber.Text = ""
		  Self.fldLateDayNumber.Text = ""
		  Self.fldBeforeTimeNumber.Text = ""
		  Self.fldBeforeDayNumber.Text = ""
		  Self.timeId = ""
		  Dim formData As New JSONItem
		  Self.formData = formData
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeleteData()
		  If Self.timeId <> "" Then
		    Dim timeInstance AS New Repo.WS.TimeTables
		    If timeInstance.Delete(Self.timeId) Then
		      MsgBox("ลบข้อมูลสำเร็จ")
		      TimeMasterWindow.cTimeTableMasterListTreeView.LoadTimeTableList
		      TimeMasterWindow.PagePanel1.Value = 0
		      Self.CleanValue
		    Else
		      MsgBox("ลบข้อมูลไม่สำเร็จ")
		    End If
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetValue() As JSONItem
		  Dim timeTablesData As new JSONItem
		  
		  timeTablesData.Value("code") = fldCode.Text
		  timeTablesData.Value("name") = fldName.Text
		  timeTablesData.Value("vacation") = chkVacation.Value
		  timeTablesData.Value("timeIn") = fldTimeIn.Text
		  timeTablesData.Value("timeInLate") = fldTimeInLate.Text
		  timeTablesData.Value("timeOut") = fldTimeOut.Text
		  timeTablesData.Value("timeOutBefore") = fldTimeOutBefore.Text
		  timeTablesData.Value("breakOut") = fldBreakOut.Text
		  timeTablesData.Value("breakIn") = fldBreakIn.Text
		  timeTablesData.Value("totalHour") = fldTotalHour.Text
		  If(rdoLateCalculate.Value) Then
		    timeTablesData.Value("lateCalculate") = false
		    timeTablesData.Value("lateTimeCalculate") = false
		    timeTablesData.Value("lateTimeNumber") = 0
		    timeTablesData.Value("lateDayNumber") = 0
		  Else
		    timeTablesData.Value("lateCalculate") = true
		    timeTablesData.Value("lateTimeCalculate") = true
		    timeTablesData.Value("lateTimeNumber") = fldLateTimeNumber.Text
		    timeTablesData.Value("lateDayNumber") = fldLateDayNumber.Text
		  End If
		  timeTablesData.Value("lateMinuteCalculate") = false
		  timeTablesData.Value("lateMinuteConfig") = ""
		  timeTablesData.Value("latePersonnelType") = ""
		  If(rdoBeforeCalculate.Value) Then
		    timeTablesData.Value("beforeCalculate") = false
		    timeTablesData.Value("beforeTimeCalculate") = false
		    timeTablesData.Value("beforeTimeNumber") = 0
		    timeTablesData.Value("beforeDayNumber") = 0
		  Else
		    timeTablesData.Value("beforeCalculate") = true
		    timeTablesData.Value("beforeTimeCalculate") = true
		    timeTablesData.Value("beforeTimeNumber") = fldBeforeTimeNumber.Text
		    timeTablesData.Value("beforeDayNumber") = fldBeforeDayNumber.Text
		  End If
		  timeTablesData.Value("beforeMinuteCalculate") = false
		  timeTablesData.Value("beforeMinuteConfig") = ""
		  timeTablesData.Value("beforePersonnelType") = ""
		  
		  Return timeTablesData
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendData()
		  Dim timeInstance AS New Repo.WS.TimeTables
		  
		  If Self.timeId <> "" Then
		    If timeInstance.Update(Self.formData) Then
		      MsgBox("แก้ไขข้อมูลสำเร็จ")
		      TimeMasterWindow.cTimeTableMasterListTreeView.LoadTimeTableList
		    Else
		      MsgBox("แก้ไขข้อมูลไม่สำเร็จ")
		    End
		  Else
		    If timeInstance.Save(Self.formData) Then
		      MsgBox("เพิ่มข้อมูลสำเร็จ")
		      TimeMasterWindow.cTimeTableMasterListTreeView.LoadTimeTableList
		    Else
		      MsgBox("เพิ่มข้อมูลไม่สำเร็จ")
		    End
		  End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetID(timeId AS String)
		  If timeId <> "" Then
		    Self.timeId = timeId
		    
		    Dim timeTablesInstance AS New Repo.WS.TimeTables
		    Dim timeData AS New JSONItem
		    
		    timeData = timeTablesInstance.GetByID(timeID)
		    
		    Self.formData = timeData
		    SetValue(Self.formData)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue(data AS JSONItem)
		  If data.Value("status") = True Then
		    Dim objData As JSONItem = data.Value("data")
		    
		    Self.fldCode.Text = objData.Value("code").StringValue
		    Self.fldName.Text = objData.Value("name").StringValue
		    Self.fldTimeIn.Text = objData.Value("timeIn").StringValue
		    Self.fldTimeInLate.Text = objData.Value("timeInLate").StringValue
		    Self.fldTimeOut.Text = objData.Value("timeOut").StringValue
		    Self.fldTimeOutBefore.Text = objData.Value("timeOutBefore").StringValue
		    Self.fldBreakOut.Text = objData.Value("breakOut").StringValue
		    Self.fldBreakIn.Text = objData.Value("breakIn").StringValue
		    Self.fldTotalHour.text = objData.Value("totalHour").StringValue
		    Self.chkVacation.Value = objData.Value("vacation").BooleanValue
		    Self.rdoLateCalculate.Value = objData.Value("lateCalculate").BooleanValue
		    Self.rdoLateTimeCalculate.Value = objData.Value("lateTimeCalculate").BooleanValue
		    Self.rdoBeforeCalculate.Value = objData.Value("beforeCalculate").BooleanValue
		    Self.rdoBeforeTimeCalculate.Value = objData.Value("beforeTimeCalculate").BooleanValue
		    Self.fldLateTimeNumber.Text = objData.Value("lateTimeNumber").StringValue
		    Self.fldLateDayNumber.Text = objData.Value("lateDayNumber").StringValue
		    Self.fldBeforeTimeNumber.Text = objData.Value("beforeTimeNumber").StringValue
		    Self.fldBeforeDayNumber.Text = objData.Value("beforeDayNumber").StringValue
		    TimeMasterWindow.SetLabel(objData)
		  End
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		formData As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private timeId As String
	#tag EndProperty


#tag EndWindowCode

#tag Events fldTimeIn
	#tag Event
		Sub LostFocus()
		  Dim breakTotal As Double = 0
		  If (fldBreakOut.Text <> "") AND (fldBreakIn.Text <> "") Then
		    breakTotal = CalculateBreak(fldBreakOut.Text,fldBreakIn.Text)
		  End If
		  If fldTimeIn.Text <> "" AND fldTimeOut.Text <> "" Then
		    fldTotalHour.Text = Str(CalculateWorkHour(fldTimeIn.Text,fldTimeOut.Text,breakTotal))
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events fldTimeOut
	#tag Event
		Sub LostFocus()
		  Dim breakTotal As Double = 0
		  If (fldBreakOut.Text <> "") AND (fldBreakIn.Text <> "") Then
		    breakTotal = CalculateBreak(fldBreakOut.Text,fldBreakIn.Text)
		  End If
		  If fldTimeIn.Text <> "" AND fldTimeOut.Text <> "" Then
		    fldTotalHour.Text = Str(CalculateWorkHour(fldTimeIn.Text,fldTimeOut.Text,breakTotal))
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events fldBreakOut
	#tag Event
		Sub LostFocus()
		  Dim breakTotal As Double = 0
		  If (fldBreakOut.Text <> "") AND (fldBreakIn.Text <> "") Then
		    breakTotal = CalculateBreak(fldBreakOut.Text,fldBreakIn.Text)
		  End If
		  If (fldTimeIn.Text <> "") AND (fldTimeOut.Text <> "") Then
		    fldTotalHour.Text = Str(CalculateWorkHour(fldTimeIn.Text,fldTimeOut.Text,breakTotal))
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events fldBreakIn
	#tag Event
		Sub LostFocus()
		  Dim breakTotal As Double = 0
		  If (fldBreakOut.Text <> "") AND (fldBreakIn.Text <> "") Then
		    breakTotal = CalculateBreak(fldBreakOut.Text,fldBreakIn.Text)
		  End If
		  If (fldTimeIn.Text <> "") AND (fldTimeOut.Text <> "") Then
		    fldTotalHour.Text = Str(CalculateWorkHour(fldTimeIn.Text,fldTimeOut.Text,breakTotal))
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events rdoLateCalculate
	#tag Event
		Sub Action()
		  lblLateDayNumber.Enabled = False
		  lblLateTimeNumber.Enabled = False
		  fldLateDayNumber.Enabled = False
		  fldLateTimeNumber.Enabled = False
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events rdoLateTimeCalculate
	#tag Event
		Sub Action()
		  lblLateDayNumber.Enabled = True
		  lblLateTimeNumber.Enabled = True
		  fldLateDayNumber.Enabled = True
		  fldLateTimeNumber.Enabled = True
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  lblLateDayNumber.Enabled = False
		  lblLateTimeNumber.Enabled = False
		  fldLateDayNumber.Enabled = False
		  fldLateTimeNumber.Enabled = False
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events rdoBeforeTimeCalculate
	#tag Event
		Sub Action()
		  lblBeforeDayNumber.Enabled = True
		  lblBeforeTimeNumber.Enabled = True
		  fldBeforeDayNumber.Enabled = True
		  fldBeforeTimeNumber.Enabled = True
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  lblBeforeDayNumber.Enabled = False
		  lblBeforeTimeNumber.Enabled = False
		  fldBeforeDayNumber.Enabled = False
		  fldBeforeTimeNumber.Enabled = False
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events rdoBeforeCalculate
	#tag Event
		Sub Action()
		  lblBeforeDayNumber.Enabled = False
		  lblBeforeTimeNumber.Enabled = False
		  fldBeforeDayNumber.Enabled = False
		  fldBeforeTimeNumber.Enabled = False
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

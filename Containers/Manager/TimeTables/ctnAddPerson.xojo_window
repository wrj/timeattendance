#tag Window
Begin ContainerControl ctnAddPerson
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   427
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   539
   Begin Listbox lstPersonnelNoTime
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   4
      ColumnsResizable=   False
      ColumnWidths    =   "10%,20%,*,*"
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   2
      GridLinesVertical=   2
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   282
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "ลำดับ	รหัสพนักงาน	ชื่อ - นามสกุล	ตำแหน่ง"
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   91
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   499
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin SearchControl SearchControl1
      AutoDeactivate  =   True
      Enabled         =   True
      HasCancelButton =   True
      HasMenu         =   False
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      PlaceHolderText =   ""
      Scope           =   0
      SendSearchStringImmediately=   False
      SendWholeSearchString=   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      Top             =   66
      Visible         =   True
      Width           =   407
   End
   Begin Label lblHeader
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   34
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      Text            =   "เพิ่มพนักงาน"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   24.0
      TextUnit        =   1
      Top             =   20
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   499
   End
   Begin PushButton btnSave
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ตกลง"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   439
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   385
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton btnCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ยกเลิก"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   347
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   385
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton btnSearch
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ค้นหา"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   439
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   66
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Sub BindData()
		  formData = Me.GetValue
		  formData.Value("timeId") = Self.timeId
		  //MsgBox(formData.ToString)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetValue() As JSONItem
		  Dim newPersonnel AS New JSONItem
		  Dim pData As New JSONItem
		  Dim pArray AS New JSONItem
		  
		  Dim data As New JSONItem
		  data.Value("owner") = lstPersonnelNoTime.CellTag(lstPersonnelNoTime.ListIndex,0)
		  data.Value("remark") = ""
		  
		  pArray.Append(data)
		  pData.Value("new") = pArray
		  
		  newPersonnel.Value("personnel") = pData
		  
		  Return newPersonnel
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadPersonnelNoTime()
		  Dim pstInstance AS New Repo.WS.PersonnelTimes
		  Dim jsData As JSONItem
		  Dim js As String
		  
		  Self.lstPersonnelNoTime.DeleteAllRows
		  
		  js = pstInstance.ListAllPersonnelNoTime(Self.organizeId,SearchControl1.Text)
		  jsData = New JSONItem(js)
		  
		  If jsData.Value("status") = True Then
		    Dim objData As JSONItem = jsData.Value("data")
		    For i As Integer = 0 To objData.Count - 1
		      Dim iItem As JSONItem = objData.Child(i)
		      lstPersonnelNoTime.AddRow()
		      lstPersonnelNoTime.CellTag(lstPersonnelNoTime.LastIndex,0) = iItem.Value("personnel_id")
		      lstPersonnelNoTime.Cell(lstPersonnelNoTime.LastIndex,0) = Str(i+1)
		      lstPersonnelNoTime.Cell(lstPersonnelNoTime.LastIndex,1) = iItem.Value("personnel_code")
		      lstPersonnelNoTime.Cell(lstPersonnelNoTime.LastIndex,2) = iItem.Value("first_name_th") + "   " +  iItem.Value("last_name_th")
		      lstPersonnelNoTime.Cell(lstPersonnelNoTime.LastIndex,3) = iItem.Value("position_name_th")
		    Next
		  End
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue(timeID As String,orgId As String)
		  Self.timeId = timeID
		  Self.organizeId = orgId
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ValidateFields() As Boolean
		  Dim res As Boolean = False
		  Dim errorText As String
		  Dim lblError As String
		  
		  If lstPersonnelNoTime.ListIndex < 0 Then
		    lblError = "กรุณาเลือกพนักงาน"
		    errorText = errorText + lblError+EndOfLine
		  End if
		  
		  If errorText <> "" Then
		    MsgBox errorText
		  Else
		    res = True
		  End if
		  Return res
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		formData As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		organizeId As String
	#tag EndProperty

	#tag Property, Flags = &h0
		personCode As String
	#tag EndProperty

	#tag Property, Flags = &h0
		personId As String
	#tag EndProperty

	#tag Property, Flags = &h0
		personName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		positionName As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private timeId As String
	#tag EndProperty


#tag EndWindowCode

#tag Events btnSave
	#tag Event
		Sub Action()
		  If ValidateFields Then
		    // Self.BindData
		    // Self.SendData
		    Self.personId = lstPersonnelNoTime.CellTag(lstPersonnelNoTime.ListIndex,0)
		    Self.personCode = lstPersonnelNoTime.Cell(lstPersonnelNoTime.ListIndex,1)
		    Self.personName = lstPersonnelNoTime.Cell(lstPersonnelNoTime.ListIndex,2)
		    Self.positionName = lstPersonnelNoTime.Cell(lstPersonnelNoTime.ListIndex,3)
		    // Self.close
		    Self.Window.Close
		    // TimeTableWindow.cPersonnelList.LoadPersonnelList(Self.timeId)
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnCancel
	#tag Event
		Sub Action()
		  Self.close
		  AddPersonnelWindow.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSearch
	#tag Event
		Sub Action()
		  LoadPersonnelNoTime
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="organizeId"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="personCode"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="personId"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="personName"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="positionName"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

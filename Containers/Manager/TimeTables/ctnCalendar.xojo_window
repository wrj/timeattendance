#tag Window
Begin ContainerControl ctnCalendar
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   582
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   942
   Begin ctnDay ctnDay2
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   197
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   54
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay3
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   309
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   54
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay4
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   421
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   54
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay5
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   533
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   54
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay6
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   645
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   54
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay7
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   757
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   54
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay1
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   85
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   54
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay8
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   85
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   146
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay9
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   197
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   146
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay10
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   309
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   146
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay11
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   421
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   146
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay12
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   533
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   146
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay13
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   645
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   146
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay14
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   757
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   146
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay15
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   85
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   14
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   238
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay16
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   197
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   15
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   238
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay17
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   309
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   16
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   238
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay18
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   421
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   17
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   238
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay19
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   533
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   18
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   238
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay20
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   645
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   19
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   238
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay21
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   757
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   20
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   238
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay22
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   85
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   21
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   330
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay23
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   197
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   22
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   330
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay24
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   309
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   23
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   330
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay25
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   421
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   24
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   330
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay26
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   533
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   25
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   330
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay27
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   645
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   26
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   330
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay28
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   757
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   27
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   330
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay29
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   85
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   28
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   422
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay30
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   197
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   29
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   422
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin ctnDay ctnDay31
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   80
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   309
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   30
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   422
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   100
   End
   Begin TextField fldYear
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFF00FF
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   False
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   371
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   31
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   20
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   111
   End
   Begin PopupMenu popMonth
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   False
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   145
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   33
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   20
      Underline       =   False
      Visible         =   True
      Width           =   145
   End
   Begin Label Label1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   85
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   34
      TabPanelIndex   =   0
      Text            =   "เดือน"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   20
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   48
   End
   Begin Label Label2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   309
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   35
      TabPanelIndex   =   0
      Text            =   "ปี"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   20
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   48
   End
   Begin PushButton btnSave
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "บันทึก"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   777
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   36
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   514
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton btnCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ยกเลิก"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   685
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   37
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   514
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  Self.SetUpCalendar
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub BindData()
		  Self.formData  = Self.GetValue
		  
		  MsgBox(Self.formData.ToString)
		  // Self.SendData(formData)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetValue() As JSONItem
		  Dim timeTableData AS  New JSONItem
		  Dim timeWorkData As New JSONItem
		  Dim month AS Integer
		  Dim year As Integer
		  Dim amountDay AS Integer
		  
		  month = Self.popMonth.ListIndex+1
		  year = Val(Self.fldYear.Text)
		  amountDay = Utils.DateTools.gFindDayInMonth(month, year)
		  
		  Dim timeWork AS New JSONItem
		  Dim shiftTimeData AS New JSONItem
		  
		  timeWorkData.Append(ctnDay1.GetValue())
		  timeWorkData.Append(ctnDay2.GetValue())
		  timeWorkData.Append(ctnDay3.GetValue())
		  timeWorkData.Append(ctnDay4.GetValue())
		  timeWorkData.Append(ctnDay5.GetValue())
		  timeWorkData.Append(ctnDay6.GetValue())
		  timeWorkData.Append(ctnDay7.GetValue())
		  timeWorkData.Append(ctnDay8.GetValue())
		  timeWorkData.Append(ctnDay9.GetValue())
		  timeWorkData.Append(ctnDay10.GetValue())
		  timeWorkData.Append(ctnDay11.GetValue())
		  timeWorkData.Append(ctnDay12.GetValue())
		  timeWorkData.Append(ctnDay13.GetValue())
		  timeWorkData.Append(ctnDay14.GetValue())
		  timeWorkData.Append(ctnDay15.GetValue())
		  timeWorkData.Append(ctnDay16.GetValue())
		  timeWorkData.Append(ctnDay17.GetValue())
		  timeWorkData.Append(ctnDay18.GetValue())
		  timeWorkData.Append(ctnDay19.GetValue())
		  timeWorkData.Append(ctnDay20.GetValue())
		  timeWorkData.Append(ctnDay21.GetValue())
		  timeWorkData.Append(ctnDay22.GetValue())
		  timeWorkData.Append(ctnDay23.GetValue())
		  timeWorkData.Append(ctnDay24.GetValue())
		  timeWorkData.Append(ctnDay25.GetValue())
		  timeWorkData.Append(ctnDay26.GetValue())
		  timeWorkData.Append(ctnDay27.GetValue())
		  timeWorkData.Append(ctnDay28.GetValue())
		  If Self.dayInMonth = 29 Then
		    timeWorkData.Append(ctnDay29.GetValue())
		  ElseIf Self.dayInMonth = 30 Then
		    timeWorkData.Append(ctnDay29.GetValue())
		    timeWorkData.Append(ctnDay30.GetValue())
		  Else
		    timeWorkData.Append(ctnDay29.GetValue())
		    timeWorkData.Append(ctnDay30.GetValue())
		    timeWorkData.Append(ctnDay31.GetValue())
		  End If
		  
		  timeTableData.Value("personId") = Self.personId
		  timeTableData.Value("month") = Str(month)
		  timeTableData.Value("year") = Str(year)
		  timeTableData.Value("timeWork") = timeWorkData
		  
		  Return timeTableData
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendData(data AS JSONItem)
		  Dim pstInstance As New Repo.WS.PersonnelTimes
		  
		  If pstInstance.SaveTimeToPerson(formData) Then
		    MsgBox "เพิ่มข้อมูลสำเร็จ"
		    Self.SetID(Self.personId,Self.ListTimeJson)
		    // TimeTableWindow.pgpShiftWorkList.Value = 0
		    // TimeTableWindow.pgpTreeView.Value = 0
		  Else
		    MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		  End
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetDayData(timeList As JSONItem)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetID(id AS String,timeList As JSONItem)
		  if id <> "" Then
		    Dim personnelTimeData AS New JSONItem
		    Self.personId = id
		    Self.ListTimeJson = timeList
		    personnelTimeData.Value("personnelID") = id
		    personnelTimeData.Value("month") = Str(Self.popMonth.ListIndex+1)
		    personnelTimeData.Value("year") = Self.fldYear.Text
		    
		    SetValue(personnelTimeData)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetUpCalendar()
		  //Get TimeTable
		  Dim month AS Integer
		  Dim year As Integer
		  Dim amountDay AS Integer
		  
		  //gPersonInTimeID = Me.CellTag(Me.ListIndex,0)
		  
		  popMonth.Enabled = True
		  fldYear.Enabled = True
		  
		  Dim monthName() AS String = Array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม","สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม")
		  Dim currentMonth AS  New Date
		  Dim selectIndxMonth AS Integer
		  popMonth.DeleteAllRows
		  For i AS Integer = 0 To UBound(monthName)
		    popMonth.AddRow(monthName(i))
		    popMonth.RowTag(i) = Str(i+1)
		    if((i+1) = currentMonth.Month) Then
		      selectIndxMonth = i
		    End
		  Next
		  popMonth.ListIndex = selectIndxMonth
		  
		  Dim currentYear As New Date
		  fldYear.Text = currentYear.Year.ToText
		  
		  month = popMonth.ListIndex+1
		  year = Val(fldYear.Text)
		  amountDay = Utils.DateTools.gFindDayInMonth(month, year)
		  Self.dayInMonth = amountDay
		  
		  Self.ctnDay1.lblDay.Text = "1"
		  Self.ctnDay2.lblDay.Text = "2"
		  Self.ctnDay3.lblDay.Text = "3"
		  Self.ctnDay4.lblDay.Text = "4"
		  Self.ctnDay5.lblDay.Text = "5"
		  Self.ctnDay6.lblDay.Text = "6"
		  Self.ctnDay7.lblDay.Text = "7"
		  Self.ctnDay8.lblDay.Text = "8"
		  Self.ctnDay9.lblDay.Text = "9"
		  Self.ctnDay10.lblDay.Text = "10"
		  Self.ctnDay11.lblDay.Text = "11"
		  Self.ctnDay12.lblDay.Text = "12"
		  Self.ctnDay13.lblDay.Text = "13"
		  Self.ctnDay14.lblDay.Text = "14"
		  Self.ctnDay15.lblDay.Text = "15"
		  Self.ctnDay16.lblDay.Text = "16"
		  Self.ctnDay17.lblDay.Text = "17"
		  Self.ctnDay18.lblDay.Text = "18"
		  Self.ctnDay19.lblDay.Text = "19"
		  Self.ctnDay20.lblDay.Text = "20"
		  Self.ctnDay21.lblDay.Text = "21"
		  Self.ctnDay22.lblDay.Text = "22"
		  Self.ctnDay23.lblDay.Text = "23"
		  Self.ctnDay24.lblDay.Text = "24"
		  Self.ctnDay25.lblDay.Text = "25"
		  Self.ctnDay26.lblDay.Text = "26"
		  Self.ctnDay27.lblDay.Text = "27"
		  Self.ctnDay28.lblDay.Text = "28"
		  Self.ctnDay29.lblDay.Text = "29"
		  Self.ctnDay30.lblDay.Text = "30"
		  Self.ctnDay31.lblDay.Text = "31"
		  
		  
		  If amountDay = 28 Then
		    Self.ctnDay29.Visible = False
		    Self.ctnDay30.Visible = False
		    Self.ctnDay31.Visible = False
		    
		  Elseif amountDay = 29 Then
		    Self.ctnDay29.Visible = True
		    Self.ctnDay30.Visible = False
		    Self.ctnDay31.Visible = False
		    
		  ElseIf amountDay = 30 Then
		    Self.ctnDay29.Visible = True
		    Self.ctnDay30.Visible = True
		    Self.ctnDay31.Visible = False
		    
		  Else
		    Self.ctnDay29.Visible = True
		    Self.ctnDay30.Visible = True
		    Self.ctnDay31.Visible = True
		  End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue(data AS JSONItem)
		  Dim pntInstance AS New Repo.WS.PersonnelTimes
		  Dim js AS String
		  Dim jsData AS JSONItem
		  Dim objData As JSONItem
		  Dim timeList As New JSONItem
		  timeList = Self.ListTimeJson
		  js = pntInstance.ListAllTimeInPerson(data)
		  jsData = New JSONItem(js)
		  
		  If jsData.Value("status") = True Then
		    objData = jsData.Value("data")
		  End If
		  
		  ctnDay1.SetValue("1",timeList,objData)
		  ctnDay2.SetValue("2",timeList,objData)
		  ctnDay3.SetValue("3",timeList,objData)
		  ctnDay4.SetValue("4",timeList,objData)
		  ctnDay5.SetValue("5",timeList,objData)
		  ctnDay6.SetValue("6",timeList,objData)
		  ctnDay7.SetValue("7",timeList,objData)
		  ctnDay8.SetValue("8",timeList,objData)
		  ctnDay9.SetValue("9",timeList,objData)
		  ctnDay10.SetValue("10",timeList,objData)
		  ctnDay11.SetValue("11",timeList,objData)
		  ctnDay12.SetValue("12",timeList,objData)
		  ctnDay13.SetValue("13",timeList,objData)
		  ctnDay14.SetValue("14",timeList,objData)
		  ctnDay15.SetValue("15",timeList,objData)
		  ctnDay16.SetValue("16",timeList,objData)
		  ctnDay17.SetValue("17",timeList,objData)
		  ctnDay18.SetValue("18",timeList,objData)
		  ctnDay19.SetValue("19",timeList,objData)
		  ctnDay20.SetValue("20",timeList,objData)
		  ctnDay21.SetValue("21",timeList,objData)
		  ctnDay22.SetValue("22",timeList,objData)
		  ctnDay23.SetValue("23",timeList,objData)
		  ctnDay24.SetValue("24",timeList,objData)
		  ctnDay25.SetValue("25",timeList,objData)
		  ctnDay26.SetValue("26",timeList,objData)
		  ctnDay27.SetValue("27",timeList,objData)
		  ctnDay28.SetValue("28",timeList,objData)
		  If Self.dayInMonth = 28 Then
		    Self.ctnDay29.Visible = False
		    Self.ctnDay30.Visible = False
		    Self.ctnDay31.Visible = False
		  ElseIf Self.dayInMonth = 29 Then
		    ctnDay29.SetValue("29",timeList,objData)
		    Self.ctnDay29.Visible = True
		    Self.ctnDay30.Visible = False
		    Self.ctnDay31.Visible = False
		  ElseIf Self.dayInMonth = 30 Then
		    ctnDay29.SetValue("29",timeList,objData)
		    ctnDay30.SetValue("30",timeList,objData)
		    Self.ctnDay29.Visible = True
		    Self.ctnDay30.Visible = True
		    Self.ctnDay31.Visible = False
		  Else
		    ctnDay29.SetValue("29",timeList,objData)
		    ctnDay30.SetValue("30",timeList,objData)
		    ctnDay31.SetValue("31",timeList,objData)
		    Self.ctnDay29.Visible = True
		    Self.ctnDay30.Visible = True
		    Self.ctnDay31.Visible = True
		  End If
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		dayInMonth As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		formData As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private ListTimeJson As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private personId As String
	#tag EndProperty


#tag EndWindowCode

#tag Events fldYear
	#tag Event
		Function KeyDown(Key As String) As Boolean
		  If Key.Asc = 13 Then
		    If Me.Text <> "" Then
		      Dim month As Integer = popMonth.ListIndex+1
		      Dim year As Integer = Val(fldYear.Text)
		      Dim amountDay As Integer = Utils.DateTools.gFindDayInMonth(month, year)
		      Self.dayInMonth = amountDay
		      Self.SetID(Self.personId,Self.ListTimeJson)
		    End If
		  End If
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events popMonth
	#tag Event
		Sub Change()
		  Dim month As Integer = popMonth.ListIndex+1
		  Dim year As Integer = Val(fldYear.Text)
		  Dim amountDay As Integer = Utils.DateTools.gFindDayInMonth(month, year)
		  Self.dayInMonth = amountDay
		  Self.SetID(Self.personId,Self.ListTimeJson)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSave
	#tag Event
		Sub Action()
		  Self.BindData
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnCancel
	#tag Event
		Sub Action()
		  Self.SetID(Self.personId,Self.ListTimeJson)
		  // TimeTableWindow.pgpTreeView.Value = 0
		  // TimeTableWindow.pgpShiftWorkList.Value = 0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="dayInMonth"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

#tag Window
Begin ContainerControl ctnPersonnelList
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   582
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   942
   Begin Listbox lstAllPersonnelTimes
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   4
      ColumnsResizable=   False
      ColumnWidths    =   "5%,10%,*,*"
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   2
      GridLinesVertical=   2
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   487
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "ลำดับ	รหัสพนักงาน	ชื่อ-นามสกุล	ตำแหน่ง	รวม	OT	"
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   43
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   902
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin PushButton btnDelete
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ลบ"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   842
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   18
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton btnAdd
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "เพิ่ม"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   750
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   18
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton btnSave
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "บันทึก"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   842
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   542
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Sub BindData()
		  Self.GetValue()
		  Self.SendData()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ClearAll()
		  lstAllPersonnelTimes.DeleteAllRows
		  timeId = ""
		  btnAdd.Enabled = false
		  btnDelete.Enabled = false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetValue()
		  Dim formData As New JSONItem
		  Dim personnel As New JSONItem
		  Dim newPersonArray As New JSONItem
		  Dim deletePersonArray As New JSONItem
		  If Self.newPerson.Ubound <> -1 Then
		    For i As Integer = 0 to Self.newPerson.Ubound
		      Dim itemData As New JSONItem
		      itemData.Value("owner") = newPerson(i)
		      newPersonArray.Append(itemData)
		    Next
		    personnel.Value("new") = newPersonArray
		  End If
		  If Self.deletePerson.Ubound <> -1 Then
		    For i As Integer = 0 to Self.deletePerson.Ubound
		      deletePersonArray.Append(deletePerson(i))
		    Next
		    personnel.Value("delete") = deletePersonArray
		  End If
		  formData.Value("personnel") = personnel
		  formData.Value("timeId") = Self.timeId
		  
		  Self.formData = formData
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadPersonnelList(timeID AS string,orgId As String)
		  Dim pstInstance AS New Repo.WS.PersonnelTimes
		  Dim jsData As JSONItem
		  Dim js As String
		  Self.timeId = timeID
		  Self.organizaId = orgId
		  Self.btnAdd.Enabled = true
		  Self.btnDelete.Enabled = true
		  Self.lstAllPersonnelTimes.DeleteAllRows
		  
		  js = pstInstance.ListAllPersonInTime(timeID,orgId)
		  jsData = New JSONItem(js)
		  
		  If jsData.Value("status") = True Then
		    Dim objData As JSONItem = jsData.Value("data")
		    For i As Integer = 0 To objData.Count - 1
		      Dim iItem As JSONItem = objData.Child(i)
		      lstAllPersonnelTimes.AddRow()
		      lstAllPersonnelTimes.RowTag(lstAllPersonnelTimes.LastIndex) = iItem.Value("id")
		      lstAllPersonnelTimes.CellTag(lstAllPersonnelTimes.LastIndex,0) = iItem.Value("personnel_id")
		      lstAllPersonnelTimes.Cell(lstAllPersonnelTimes.LastIndex,0) = Str(i+1)
		      lstAllPersonnelTimes.Cell(lstAllPersonnelTimes.LastIndex,1) = iItem.Value("personnel_code")
		      lstAllPersonnelTimes.Cell(lstAllPersonnelTimes.LastIndex,2) = iItem.Value("full_name")
		      lstAllPersonnelTimes.Cell(lstAllPersonnelTimes.LastIndex,3) = iItem.Value("position_name_th")
		    Next
		  End
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendData()
		  Dim pstInstance As New Repo.WS.PersonnelTimes
		  
		  If pstInstance.SavePersonToTime(Self.formData) Then
		    MsgBox "เพิ่มข้อมูลสำเร็จ"
		    Dim formData As New JSONItem
		    Self.formData = formData
		    Redim Self.newPerson(-1)
		    Redim Self.deletePerson(-1)
		    Self.LoadPersonnelList(Self.timeId,Self.organizaId)
		  Else
		    MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		  End
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		deletePerson() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		formData As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		newPerson() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		organizaId As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private timeId As String
	#tag EndProperty


#tag EndWindowCode

#tag Events lstAllPersonnelTimes
	#tag Event
		Sub Change()
		  If me.ListIndex <> -1 Then
		    Self.btnDelete.Enabled = True
		  Else
		    Self.btnDelete.Enabled = False
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnDelete
	#tag Event
		Sub Action()
		  Dim delId As String = lstAllPersonnelTimes.RowTag(lstAllPersonnelTimes.ListIndex)
		  If delId <> "" Then
		    deletePerson.Append(delId)
		  Else
		    Dim newIndex As Integer
		    newIndex = newPerson.IndexOf(lstAllPersonnelTimes.CellTag(lstAllPersonnelTimes.ListIndex,0))
		    newPerson.Remove(newIndex)
		  End If
		  lstAllPersonnelTimes.RemoveRow(lstAllPersonnelTimes.ListIndex)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnAdd
	#tag Event
		Sub Action()
		  Dim aPersonWin As New AddPersonnelWindow
		  aPersonWin.SetValue(Self.timeId,Self.organizaId)
		  aPersonWin.ShowModal
		  Dim personId As String = aPersonWin.personId
		  Dim personCode As String = aPersonWin.personCode
		  Dim personName As String = aPersonWin.personName
		  Dim positionName As String = aPersonWin.positionName
		  
		  Dim newId As Boolean = True
		  For i As Integer = 0 to newPerson.Ubound
		    If newPerson(i) = personId Then
		      newId = False
		      Exit
		    End If
		  Next
		  If newId Then
		    newPerson.Append(personId)
		    Dim lastId As Integer = Val(lstAllPersonnelTimes.Cell(lstAllPersonnelTimes.LastIndex,0))
		    lstAllPersonnelTimes.AddRow()
		    lstAllPersonnelTimes.RowTag(lstAllPersonnelTimes.LastIndex) = ""
		    lstAllPersonnelTimes.CellTag(lstAllPersonnelTimes.LastIndex,0) = personId
		    lstAllPersonnelTimes.Cell(lstAllPersonnelTimes.LastIndex,0) = Str(lastId+1)
		    lstAllPersonnelTimes.Cell(lstAllPersonnelTimes.LastIndex,1) = personCode
		    lstAllPersonnelTimes.Cell(lstAllPersonnelTimes.LastIndex,2) = personName
		    lstAllPersonnelTimes.Cell(lstAllPersonnelTimes.LastIndex,3) = positionName
		  End If 
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSave
	#tag Event
		Sub Action()
		  Self.BindData
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="organizaId"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

#tag Window
Begin ContainerControl ctnPersonnelWorkRecordListTreeView
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   652
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   True
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   300
   Begin Rectangle Rectangle1
      AutoDeactivate  =   True
      BorderWidth     =   1
      BottomRightColor=   &c00000000
      Enabled         =   True
      FillColor       =   &cFFFF00FF
      Height          =   652
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      Top             =   0
      TopLeftColor    =   &c00000000
      Visible         =   True
      Width           =   300
      Begin Label Label2
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   8
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   0
         TabPanelIndex   =   0
         Text            =   "องค์กร"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   62
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   81
      End
      Begin SearchControl SearchControlPersonnels
         AutoDeactivate  =   True
         Enabled         =   True
         HasCancelButton =   True
         HasMenu         =   False
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Left            =   8
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         PlaceHolderText =   ""
         Scope           =   0
         SendSearchStringImmediately=   False
         SendWholeSearchString=   False
         TabIndex        =   2
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "806006"
         Top             =   96
         Visible         =   True
         Width           =   220
      End
      Begin PopupMenu popOrg
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         InitialValue    =   ""
         Italic          =   False
         Left            =   101
         ListIndex       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   62
         Underline       =   False
         Visible         =   True
         Width           =   191
      End
      Begin PushButton btnSearch
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "ค้นหา"
         Default         =   False
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   240
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   96
         Underline       =   False
         Visible         =   True
         Width           =   52
      End
   End
   Begin Rectangle Rectangle2
      AutoDeactivate  =   True
      BorderWidth     =   1
      BottomRightColor=   &c00000000
      Enabled         =   True
      FillColor       =   &cFFFF00FF
      Height          =   50
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      Top             =   0
      TopLeftColor    =   &c00000000
      Visible         =   True
      Width           =   300
   End
   Begin Label lblTitle
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   50
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      Text            =   ""
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   16.0
      TextUnit        =   0
      Top             =   0
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   284
   End
   Begin Listbox lstPersonnelInTime
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   3
      ColumnsResizable=   False
      ColumnWidths    =   "15%,30%,*"
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   25
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   2
      GridLinesVertical=   2
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   531
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "ลำดับ	รหัสพนักงาน	ชื่อ - นามสกุล		"
      Italic          =   False
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   121
      Underline       =   False
      UseFocusRing    =   False
      Visible         =   True
      Width           =   300
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Sub FindPersonnel(personnelCode AS String, orgID AS String)
		  Dim personnelInstance AS New Repo.WS.Personnels
		  Dim js AS String
		  Dim jsData AS JSONItem
		  js = personnelInstance.Search(personnelCode, orgID )
		  
		  //MsgBox(pData)
		  lstPersonnelInTime.DeleteAllRows
		  jsData = New JSONItem(js)
		  If jsData.Value("status") = True Then
		    Dim objData As JSONItem = jsData.Value("data")
		    For i As Integer = 0 To objData.Count - 1
		      Dim iItem As JSONItem = objData.Child(i)
		      lstPersonnelInTime.AddRow()
		      lstPersonnelInTime.CellTag(lstPersonnelInTime.LastIndex,0) = iItem.Value("id")
		      lstPersonnelInTime.Cell(lstPersonnelInTime.LastIndex,0) = Str(i+1)
		      lstPersonnelInTime.Cell(lstPersonnelInTime.LastIndex,1) = iItem.Value("code")
		      lstPersonnelInTime.Cell(lstPersonnelInTime.LastIndex,2) = iItem.Value("firstNameTh") + "   " +  iItem.Value("lastNameTh")
		      
		    Next
		  End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadOrgList()
		  Dim cpnInstance As New Repo.WS.Organize
		  Dim js As String
		  Dim jsData As JSONItem
		  
		  popOrg.DeleteAllRows
		  js = cpnInstance.ListAll()
		  jsData = New JSONItem(js)
		  If jsData.Value("status") = True Then
		    Dim objData As JSONItem = jsData.Value("data")
		    For i As Integer = 0 To objData.Count - 1
		      Dim iItem As JSONItem = objData.Child(i)
		      popOrg.AddRow(iItem.Value("nameTh"))
		      popOrg.RowTag(i) = iItem.Value("id")
		    Next
		    popOrg.ListIndex = 0
		  End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadPersonnelList(id AS String)
		  // Dim psnInstance AS New Repo.WS.Personnels
		  // Dim jsData As JSONItem
		  // Dim js As String
		  // Self.currentID = id
		  // 
		  // Self.lstPersonnelInTime.DeleteAllRows
		  // 
		  // js = psnInstance.ListAll()
		  // jsData = New JSONItem(js)
		  // 
		  // If jsData.Value("status") = True Then
		  // Dim objData As JSONItem = jsData.Value("data")
		  // For i As Integer = 0 To objData.Count - 1
		  // Dim iItem As JSONItem = objData.Child(i)
		  // lstPersonnelInTime.AddRow()
		  // lstPersonnelInTime.CellTag(lstPersonnelInTime.LastIndex,0) = iItem.Value("id")
		  // lstPersonnelInTime.Cell(lstPersonnelInTime.LastIndex,0) = Str(i+1)
		  // lstPersonnelInTime.Cell(lstPersonnelInTime.LastIndex,1) = iItem.Value("code")
		  // lstPersonnelInTime.Cell(lstPersonnelInTime.LastIndex,2) = iItem.Value("firstNameTh") + "   " +  iItem.Value("lastNameTh")
		  // 
		  // Next
		  // End
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private orgId As String
	#tag EndProperty


#tag EndWindowCode

#tag Events popOrg
	#tag Event
		Sub Change()
		  //gOrgID = "LORG_00001"
		  //Dim shiftTimeData AS JSONItem = Me.RowTag(Me.ListIndex)
		  Self.orgId = Me.RowTag(Me.ListIndex)
		  // LoadPersonnelList(Me.RowTag(Me.ListIndex))
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSearch
	#tag Event
		Sub Action()
		  Self.FindPersonnel(SearchControlPersonnels.Text, Self.orgId)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lstPersonnelInTime
	#tag Event
		Sub Change()
		  If Me.ListIndex >= 0 Then
		    ReportWorkRecordWindow.cPersonnelWorkRecordReportList.SetValue(Self.orgId,Me.CellTag(Me.ListIndex,0),Me.Cell(Me.ListIndex,1))
		    // TimeCalendarWindow.cCalendar.Visible = True
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

#tag Window
Begin ContainerControl ctnPersonnelWorkRecordReportList
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   582
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   942
   Begin Listbox lstWorkRecord
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   7
      ColumnsResizable=   False
      ColumnWidths    =   "10%,15%,15%,10%,10%,20%,20%"
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   2
      GridLinesVertical=   2
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   456
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "วันที่	เข้า	ออก	สาย	ออกก่อน	กะ	ประเภทการลา"
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   43
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   902
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin PushButton btnDelete
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ลบ"
      Default         =   False
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   842
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   9
      Underline       =   False
      Visible         =   False
      Width           =   80
   End
   Begin PushButton btnSave
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "บันทึก"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   842
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   542
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin TextField fldStartDate
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   101
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "01092016"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   9
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   109
   End
   Begin Label lblStartDate
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   6
      TabPanelIndex   =   0
      Text            =   "ตั้งแต่วันที่"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   11
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin Label lblEndDate
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   298
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      Text            =   "ถึงวันที่"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   12
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin TextField fldEndDate
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   379
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "30092016"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   10
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   109
   End
   Begin PushButton btnSearch
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ค้นหา"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   576
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   10
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton btnRecalculate
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "คำนวนเวลาใหม่"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   707
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   542
      Underline       =   False
      Visible         =   True
      Width           =   123
   End
   Begin PushButton btnPrint
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "พิมพ์รายงาน"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   593
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   542
      Underline       =   False
      Visible         =   True
      Width           =   102
   End
   Begin Label lblResultReport
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   15
      TabPanelIndex   =   0
      Text            =   ""
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   513
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   902
   End
   Begin PushButton btnCalStart
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ปฎิทิน"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   222
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   16
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   11
      Underline       =   False
      Visible         =   True
      Width           =   64
   End
   Begin PushButton btnCalEnd
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ปฎิทิน"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   500
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   17
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   10
      Underline       =   False
      Visible         =   True
      Width           =   64
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Sub BindData()
		  Self.GetValue()
		  Self.SendData()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ClearAll()
		  lstWorkRecord.DeleteAllRows
		  btnDelete.Enabled = false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetValue()
		  Dim formData As New JSONItem
		  formData.Value("personId") = Self.personId
		  formData.Value("personCode") = Self.personCode
		  formData.Value("startDate") = fldStartDate.Text
		  formData.Value("endDate") = fldEndDate.Text
		  formData.Value("personCode") = Self.personCode
		  formData.Value("time") = Self.formData
		  Self.formData = formData
		  // Dim formData As New JSONItem
		  // Dim personnel As New JSONItem
		  // Dim newPersonArray As New JSONItem
		  // Dim deletePersonArray As New JSONItem
		  // If Self.newPerson.Ubound <> -1 Then
		  // For i As Integer = 0 to Self.newPerson.Ubound
		  // Dim itemData As New JSONItem
		  // itemData.Value("owner") = newPerson(i)
		  // newPersonArray.Append(itemData)
		  // Next
		  // personnel.Value("new") = newPersonArray
		  // End If
		  // If Self.deletePerson.Ubound <> -1 Then
		  // For i As Integer = 0 to Self.deletePerson.Ubound
		  // deletePersonArray.Append(deletePerson(i))
		  // Next
		  // personnel.Value("delete") = deletePersonArray
		  // End If
		  // formData.Value("personnel") = personnel
		  // formData.Value("timeId") = Self.timeId
		  // 
		  // Self.formData = formData
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadWorkRecordList()
		  if Self.fldStartDate.Text <> "" AND Self.fldEndDate.Text <> "" AND Self.personCode <> "" Then
		    Dim pstInstance AS New Repo.WS.WorkRecords
		    Dim jsData As JSONItem
		    Dim js As String
		    Self.btnDelete.Enabled = true
		    Self.lstWorkRecord.DeleteAllRows
		    
		    js = pstInstance.ListAll(Self.personCode,Self.fldStartDate.Text,Self.fldEndDate.Text)
		    jsData = New JSONItem(js)
		    
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Dim resultData As JSONItem = jsData.Value("result")
		      For i As Integer = 0 To objData.Count - 1
		        Dim iItem As JSONItem = objData.Child(i)
		        lstWorkRecord.AddRow()
		        lstWorkRecord.RowTag(lstWorkRecord.LastIndex) = iItem.Value("id")
		        lstWorkRecord.CellTag(lstWorkRecord.LastIndex,0) = iItem.Value("logDate")
		        lstWorkRecord.Cell(lstWorkRecord.LastIndex,0) = iItem.Value("logDateFormat")
		        lstWorkRecord.Cell(lstWorkRecord.LastIndex,1) = iItem.Value("timeIn")
		        lstWorkRecord.Cell(lstWorkRecord.LastIndex,2) = iItem.Value("timeOut")
		        lstWorkRecord.Cell(lstWorkRecord.LastIndex,3) = iItem.Value("lateMin")
		        lstWorkRecord.Cell(lstWorkRecord.LastIndex,4) = iItem.Value("beforeMin")
		        lstWorkRecord.Cell(lstWorkRecord.LastIndex,5) = iItem.Value("workName")
		        lstWorkRecord.Cell(lstWorkRecord.LastIndex,6) = iItem.Value("remark")
		      Next
		      lblResultReport.Text = "สายจำนวน "+resultData.Value("totalLateTime")+" ครั้ง "+resultData.Value("totalLateMin")+" นาที ออกก่อนจำนวน "+resultData.Value("totalBeforeTime")+" ครั้ง "+resultData.Value("totalBeforeMin")+" นาที"
		    End
		  End If
		  Dim formData As New JSONItem
		  Self.formData = formData
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Print()
		  Dim pstInstance AS New Repo.WS.WorkRecords
		  Dim jsData As JSONItem
		  Dim js As String
		  
		  
		  js = pstInstance.ListAll(Self.personCode,Self.fldStartDate.Text,Self.fldEndDate.Text)
		  jsData = New JSONItem(js)
		  
		  If jsData.Value("status") = True Then
		    dim f as FolderItem
		    Dim s As String
		    Dim bs As BinaryStream
		    
		    f = GetSaveFolderItem("text/plain","WorkRecord.csv")
		    
		    if f = nil then exit sub
		    
		    bs = BinaryStream.Create(f, True)
		    Dim objData As JSONItem = jsData.Value("csv")
		    For i As Integer = 0 To objData.Count - 1
		      s=s+objData(i)+EndOfLine
		    Next
		    If bs <> Nil Then
		      //write the contents of the editField
		      bs.Write(s.ConvertEncoding (Encodings.UTF8))
		      
		      //close the binaryStream
		      bs.Close
		    End if
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		  End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReCalculate()
		  Dim pstInstance As New Repo.WS.WorkRecords
		  
		  Self.GetValue()
		  If pstInstance.Recalculate(Self.formData) Then
		    MsgBox "คำนวนใหม่สำเร็จ"
		    Dim formData As New JSONItem
		    Self.formData = formData
		    Self.LoadWorkRecordList()
		  Else
		    MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		  End
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendData()
		  Dim pstInstance As New Repo.WS.WorkRecords
		  
		  If pstInstance.Update(Self.formData) Then
		    MsgBox "แก้ไขข้อมูลสำเร็จ"
		    Dim formData As New JSONItem
		    Self.formData = formData
		    Self.LoadWorkRecordList()
		  Else
		    MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		  End
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue(orgId As String,personID As String,gPersonCode As String)
		  Self.personId = personID
		  Self.organizaId = orgId
		  Self.personCode = gPersonCode
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		formData As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		organizaId As String
	#tag EndProperty

	#tag Property, Flags = &h0
		personCode As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private personId As String
	#tag EndProperty


#tag EndWindowCode

#tag Events lstWorkRecord
	#tag Event
		Sub DoubleClick()
		  If Me.ListIndex <> -1 Then
		    If Me.RowTag(Me.ListIndex) <> "" Then
		      Dim fWin As New FormWorkRecordWindow
		      fWin.SetValue(Me.CellTag(Me.ListIndex,0),Me.Cell(Me.ListIndex,1),Me.Cell(Me.ListIndex,2))
		      fWin.ShowModal
		      Dim tIn As String = fWin.tiMeIn
		      Dim tOut As String = fWin.tiMeOut
		      If tIn <> "" AND tOut <> "" Then
		        Me.Cell(Me.ListIndex,1) = tIn
		        Me.Cell(Me.ListIndex,2) = tOut
		        Dim upItem As New JSONItem
		        upItem.Value("id") = Me.RowTag(Me.ListIndex)
		        upItem.Value("logDate") = Me.CellTag(Me.ListIndex,0)
		        upItem.Value("timeIn") = tIn
		        upItem.Value("timeOut") = tOut
		        Self.formData.Append(upItem)
		      End If
		    End If
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnDelete
	#tag Event
		Sub Action()
		  // Dim delId As String = lstWorkRecord.RowTag(lstWorkRecord.ListIndex)
		  // If lstWorkRecord.RowTag(lstWorkRecord.ListIndex) <> "" Then
		  // deleteWorkRecord.Append(lstWorkRecord.RowTag(lstWorkRecord.ListIndex))
		  // lstWorkRecord.RemoveRow(lstWorkRecord.ListIndex)
		  // End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSave
	#tag Event
		Sub Action()
		  Self.BindData
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSearch
	#tag Event
		Sub Action()
		  Self.LoadWorkRecordList()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnRecalculate
	#tag Event
		Sub Action()
		  Self.ReCalculate
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnPrint
	#tag Event
		Sub Action()
		  Self.Print
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnCalStart
	#tag Event
		Sub Action()
		  Dim selDay As New Date
		  If Len(fldStartDate.Text) = 8 AND Val(Mid(fldStartDate.Text,1,2)) <> 0 AND Val(Mid(fldStartDate.Text,3,2)) <> 0 AND Val(Mid(fldStartDate.Text,5,4)) <> 0 Then
		    selDay.Day = Val(Mid(fldStartDate.Text,1,2))
		    selDay.Month = Val(Mid(fldStartDate.Text,3,2))
		    selDay.Year = Val(Mid(fldStartDate.Text,5,4))
		  End If
		  Dim calWin As New CalendarWindow
		  calWin.Top = Self.Window.Top+Self.Top+Me.Top
		  calWin.Left = Self.Window.Left+Self.Left+Me.Left
		  calWin.SetDate(selDay)
		  calWin.ShowModal
		  If calWin.day <> "" Then
		    fldStartDate.Text = calWin.day+""+calWin.month+""+calWin.year
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnCalEnd
	#tag Event
		Sub Action()
		  Dim selDay As New Date
		  If Len(fldEndDate.Text) = 8 AND Val(Mid(fldEndDate.Text,1,2)) <> 0 AND Val(Mid(fldEndDate.Text,3,2)) <> 0 AND Val(Mid(fldEndDate.Text,5,4)) <> 0 Then
		    selDay.Day = Val(Mid(fldEndDate.Text,1,2))
		    selDay.Month = Val(Mid(fldEndDate.Text,3,2))
		    selDay.Year = Val(Mid(fldEndDate.Text,5,4))
		  End If
		  Dim calWin As New CalendarWindow
		  calWin.Top = Self.Window.Top+Self.Top+Me.Top
		  calWin.Left = Self.Window.Left+Self.Left+Me.Left
		  calWin.SetDate(selDay)
		  calWin.ShowModal
		  If calWin.day <> "" Then
		    fldEndDate.Text = calWin.day+""+calWin.month+""+calWin.year
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="organizaId"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="personCode"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

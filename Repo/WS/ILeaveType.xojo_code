#tag Interface
Protected Interface ILeaveType
	#tag Method, Flags = &h0
		Function Delete(id as String) As Boolean
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetByID(id as String) As JSONItem
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll() As String
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(formData as JSONItem) As Boolean
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(formData as JSONItem) As Boolean
		  
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Interface
#tag EndInterface

#tag Class
Protected Class Organize
Implements IOrganize
	#tag Method, Flags = &h0
		Function Delete(Id As String) As Boolean
		  //Dim c As New CURLSMBS
		  //Dim response As Integer
		  //Dim rawData As JSONItem
		  //Dim httpHeader() As String
		  //
		  //c.OptionVerbose=True
		  //c.OptionHeader = False
		  //c.CollectDebugData = True
		  //c.CollectOutputData = True
		  //httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  //c.SetOptionHTTPHeader(httpHeader)
		  //c.OptionCustomRequest = "DELETE"
		  //
		  //c.OptionURL = App.kURLService + Self.NameService + "/"+Id
		  //response = c.Perform
		  //If response = 0 Then
		  //rawData = New JSONItem(c.OutputData)
		  //If rawData.Value("status") Then
		  //Return True
		  //End
		  //End
		  //Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetByID(id As String) As JSONItem
		  //Dim c As New CURLSMBS
		  //Dim response As Integer
		  //Dim httpHeader() As String
		  //
		  //c.OptionVerbose=True
		  //c.OptionURL = App.kURLService + NameService + "/" + id
		  //c.OptionHeader = False
		  //httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  //httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  //c.SetOptionHTTPHeader(httpHeader)
		  //c.CollectOutputData = True
		  //response = c.Perform
		  //Dim dataR As New JSONItem
		  //
		  //If response = 0 Then
		  //Dim reqData As New JSONItem(c.OutputData)
		  //if reqData.Value("status") = true Then
		  //Dim objData As JSONItem = reqData.Value("data")
		  //dataR.Value("id") = objData.Value("id")
		  //dataR.Value("noTaxpayers") = objData.Value("noTaxpayers")
		  //dataR.Value("organizationTypeToText") = objData.Value("organizationTypeToText")
		  //dataR.Value("addressRoad") = objData.Value("addressRoad")
		  //dataR.Value("addressName") = objData.Value("addressName")
		  //dataR.Value("nameTh") = objData.Value("nameTh")
		  //dataR.Value("addressProvince") = objData.Value("addressProvince")
		  //dataR.Value("nameEng") = objData.Value("nameEng")
		  //dataR.Value("addressSubDistrict") = objData.Value("addressSubDistrict")
		  //dataR.Value("telephone1") = objData.Value("telephone1")
		  //dataR.Value("founderDate") = objData.Value("founderDate")
		  //dataR.Value("addressMap") = objData.Value("addressMap")
		  //dataR.Value("fax") = objData.Value("fax")
		  //dataR.Value("addressProvinceId") = objData.Value("addressProvinceId")
		  //dataR.Value("addressNo") = objData.Value("addressNo")
		  //dataR.Value("addressDistrict") = objData.Value("addressDistrict")
		  //dataR.Value("addressAlley") = objData.Value("addressAlley")
		  //dataR.Value("parentOrganizationId") = objData.Value("parentOrganizationId")
		  //dataR.Value("organizationType") = objData.Value("organizationType")
		  //dataR.Value("parentOrganization") = objData.Value("parentOrganization")
		  //dataR.Value("email") = objData.Value("email")
		  //dataR.Value("remark") = objData.Value("remark")
		  //dataR.Value("telephone3") = objData.Value("telephone3")
		  //dataR.Value("telephone2") = objData.Value("telephone2")
		  //dataR.Value("addressZipcode") = objData.Value("addressZipcode")
		  //dataR.Value("addressZipcodeName") = objData.Value("addressZipcodeName")
		  //dataR.Value("website") = objData.Value("website")
		  //dataR.Value("addressDistrictId") = objData.Value("addressDistrictId")
		  //dataR.Value("addressSubDistrictId") = objData.Value("addressSubDistrictId")
		  //dataR.Value("logo") = objData.Value("logo")
		  //dataR.Value("addressLane") = objData.Value("addressLane")
		  //dataR.Value("branchNo") = objData.Value("branchNo")
		  //dataR.Value("code") = objData.Value("code")
		  //dataR.Value("addressSubDistrictName") = objData.Value("addressSubDistrictName")
		  //dataR.Value("addressDistrictName") = objData.Value("addressDistrictName")
		  //dataR.Value("addressProvinceName") = objData.Value("addressProvinceName")
		  //dataR.Value("fullAddress") = objData.Value("fullAddress")
		  //End
		  //End
		  //
		  //Return dataR
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll(sData As JSONItem = Nil) As String
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim data() As String
		  
		  Dim URL As String
		  URL = App.kURLService + Self.NameService + "/list"
		  c.OptionVerbose=True
		  c.OptionURL = URL
		  c.OptionHeader = False
		  data.Append("Accept-Version: " + Self.ApiVersion)
		  data.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(data)
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Return c.OutputData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(formData As JSONItem) As Boolean
		  //Dim c As New CURLSMBS
		  //Dim response As Integer
		  //Dim rawData As JSONItem
		  //Dim httpHeader() As String
		  //
		  //c.OptionVerbose=True
		  //c.OptionHeader = False
		  //c.OptionPost = True
		  //c.CollectOutputData = True
		  //httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  //httpHeader.Append("Content-Type: application/json")
		  //httpHeader.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  //c.SetOptionHTTPHeader(httpHeader)
		  //
		  //c.OptionPostFields = formData.ToString
		  //c.OptionURL = App.kURLService + NameService
		  //response = c.Perform
		  //If response = 0 Then
		  //rawData = New JSONItem(c.OutputData)
		  //If rawData.Value("status") Then
		  //Return True
		  //End
		  //End
		  //Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(formData As JSONItem) As Boolean
		  //Dim c As New CURLSMBS
		  //Dim response As Integer
		  //Dim rawData As JSONItem
		  //Dim httpHeader() As String
		  //
		  //c.OptionVerbose=True
		  //c.OptionHeader = False
		  //c.CollectOutputData = True
		  //httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  //httpHeader.Append("Content-Type: application/json")
		  //httpHeader.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  //c.SetOptionHTTPHeader(httpHeader)
		  //c.OptionCustomRequest = "PUT"
		  //c.OptionPostFields = formData.ToString
		  //c.OptionURL = App.kURLService + Self.NameService + "/" + formData.Value("id")
		  //response = c.Perform
		  //If response = 0 Then
		  //rawData = New JSONItem(c.OutputData)
		  //If rawData.Value("status") Then
		  //Return True
		  //End
		  //End
		  //Return False
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private ApiVersion As String = "1.0"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private NameService As String = "organizations"
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

#tag Class
Protected Class TimeTables
Implements ITimeTables
	#tag Method, Flags = &h0
		Function Delete(id as String) As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.OptionPost = True
		  c.CollectOutputData = True
		  
		  httpHeader.Append("Accept-Version: 1.0")
		  httpHeader.Append("Content-Type: application/json")
		  // httpHeader.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  c.SetOptionHTTPHeader(httpHeader)
		  c.OptionCustomRequest = "DELETE"
		  
		  // c.OptionPostFields = formData.ToString
		  Dim URL As String
		  URL = App.kURLService + Self.NameService +"/"+ id
		  c.OptionURL = URL
		  
		  response = c.Perform
		  If response = 0 Then
		    //MsgBox(c.OutputData)
		    rawData = New JSONItem(c.OutputData)
		    If rawData.Value("status") Then
		      Return True
		    End
		  End
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetByID(id as String) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURLService + NameService + "/" + id
		  c.OptionHeader = False
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(httpHeader)
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Dim reqData As New JSONItem(c.OutputData)
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll() As String
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim data() As String
		  
		  Dim URL As String
		  URL = App.kURLService + Self.NameService + "/list" + "?fieldSort=id"
		  c.OptionVerbose=True
		  c.OptionURL = URL
		  c.OptionHeader = False
		  data.Append("Accept-Version: " + Self.ApiVersion)
		  data.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(data)
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Return c.OutputData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ReportTimeSchdule(formData As JSONItem) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim data() As String
		  
		  Dim URL As String
		  URL = App.kURLService + "reports/timeSchdule?department="+formData.Value("department")+"&month="+formData.Value("month")+"&year"+formData.Value("year")
		  c.OptionVerbose=True
		  c.OptionURL = URL
		  c.OptionHeader = False
		  data.Append("Accept-Version: " + Self.ApiVersion)
		  data.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(data)
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  response = c.Perform
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      reqData = New JSONItem(c.OutputData)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(formData as JSONItem) As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.OptionPost = True
		  c.CollectOutputData = True
		  httpHeader.Append("Accept-Version: 1.0")
		  httpHeader.Append("Content-Type: application/json")
		  httpHeader.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  c.SetOptionHTTPHeader(httpHeader)
		  
		  c.OptionPostFields = formData.ToString
		  Dim URL As String
		  URL = App.kURLService + Self.NameService
		  c.OptionURL = URL
		  
		  response = c.Perform
		  If response = 0 Then
		    //MsgBox(c.OutputData)
		    rawData = New JSONItem(c.OutputData)
		    If rawData.Value("status") Then
		      Return True
		    End
		  End
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(formData as JSONItem) As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.CollectOutputData = True
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json")
		  httpHeader.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  c.SetOptionHTTPHeader(httpHeader)
		  c.OptionCustomRequest = "PUT"
		  c.OptionPostFields = formData.ToString
		  c.OptionURL = App.kURLService + Self.NameService + "/" + formData.Value("timeID")
		  response = c.Perform
		  If response = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    If rawData.Value("status") Then
		      Return True
		    End
		  End
		  Return False
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private ApiVersion As String = "1.0"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private NameService As String = "timeTables"
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

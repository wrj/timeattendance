#tag Module
Protected Module Utils
	#tag Method, Flags = &h0
		Sub gLogError(msg As String)
		  MsgBox msg
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MapJSONItem(data As JSONItem,nData As JSONItem) As JSONItem
		  For Each fieldName As String In nData.Names()
		    data.Value(fieldName) = nData.Value(fieldName)
		  Next
		  return data
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		gAction As String
	#tag EndProperty

	#tag Property, Flags = &h0
		gAllTimeTable As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		gBtn As String
	#tag EndProperty

	#tag Property, Flags = &h0
		gOrgID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		gPersonInTimeID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		gShiftTimeID As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="gAction"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="gBtn"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="gOrgID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="gPersonInTimeID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="gShiftTimeID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule

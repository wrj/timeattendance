#tag Module
Protected Module ImgTools
	#tag Method, Flags = &h1
		Protected Function GetExtFile(filename As String) As String
		  Dim anArray(-1) as String
		  anArray=Split(filename,".")
		  return anArray(UBound(anArray))
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function PictureToString(p As Picture,typeImage As String) As String
		  if typeImage = "PNG" or typeImage = "png" then
		    return EncodeBase64(p.GetData(Picture.FormatPNG),0)
		  else
		    return EncodeBase64(p.GetData(Picture.FormatJPEG),0)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ResizePictureHd(originalPicture As Picture) As picture
		  Dim picWidth As integer = originalPicture.Width
		  Dim picHeight As integer = originalPicture.Height
		  Dim contentWidth As Integer
		  Dim contentHeight As Integer
		  Dim newWidth As integer
		  Dim newHeight As integer
		  Dim resizePercen As String
		  Dim typePictue As String
		  if picWidth < picHeight then
		    typePictue = "portrait"
		    contentHeight = 1280
		    contentWidth = 720
		  else
		    typePictue = "landscape"
		    contentHeight = 720
		    contentWidth = 1280
		  end if
		  if picHeight > contentHeight or picWidth > contentWidth then
		    if picHeight < picWidth then
		      resizePercen = Format(picWidth/contentWidth,"###.###")
		    else
		      resizePercen = Format(picHeight/contentHeight,"###.###")
		    end if
		    newWidth = picWidth/val(resizePercen)
		    newHeight = picHeight/val(resizePercen)
		    dim pic as new Picture(contentWidth,contentHeight,32)
		    pic.Graphics.DrawPicture(originalPicture,0,0,newWidth,newHeight,0,0,picWidth,picHeight)
		    return pic
		  else
		    return originalPicture
		  end if
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function StringToPicture(data As String) As Picture
		  '// Convert a String to a picture
		  Dim p As picture
		  If data="" Then Return Nil
		  p=picture.FromData(DecodeBase64(data))
		  Return p
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ThumbnailPicture(originalPicture As Picture,contentWidth As Integer,contentHeight As Integer) As Picture
		  Dim picWidth As Double = originalPicture.Width
		  Dim picHeight As Double = originalPicture.Height
		  Dim newWidth As Double
		  Dim newHeight As Double
		  Dim resizePercen As Double
		  if picHeight < picWidth then
		    resizePercen = (picWidth/contentWidth) + 0.2
		  else
		    resizePercen = (picHeight/contentHeight) + 0.2
		  end if
		  newWidth = picWidth/resizePercen
		  newHeight = picHeight/resizePercen
		  dim pic as new Picture(contentWidth,contentHeight,32)
		  pic.Graphics.DrawPicture(originalPicture,7,7,newWidth,newHeight,0,0,picWidth,picHeight)
		  return pic
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule

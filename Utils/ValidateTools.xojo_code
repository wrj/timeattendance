#tag Module
Protected Module ValidateTools
	#tag Method, Flags = &h1
		Protected Function gValidateEmail(emailTxt As String) As Boolean
		  Dim result As Boolean
		  
		  Dim re As New RegEx
		  Dim rm As New RegExMatch
		  
		  If emailTxt = "" then
		    MsgBox "Input your Email"
		  else
		    re.SearchPattern = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
		    rm =re.Search(emailTxt)
		    
		    if rm = Nil Then
		      'MsgBox "Email worng pattern."
		      result = False
		    Else
		      result = True
		    End
		  End
		  
		  return result
		  
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule

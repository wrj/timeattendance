#tag Module
Protected Module WebService
	#tag Method, Flags = &h1
		Protected Function InfoResponseCode(responseCode As Integer) As JSONItem
		  Dim result As New JSONItem
		  Select Case responseCode
		  Case 200
		    result.Value("status") = true
		  Case 400
		    result.Value("status") = false
		    result.Value("message") = "Bad Request."
		  Case 401
		    result.Value("status") = false
		    result.Value("message") = "Username or Password wrong."
		  Case 403
		    result.Value("status") = false
		    result.Value("message") = "Sorry, you're not authorized."
		  Case 302
		    result.Value("status") = false
		    result.Value("message") = "Wrong Token."
		  End Select
		  Return result
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function IsConnect() As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURLService 
		  c.OptionHeader = False
		  httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(httpHeader)
		  c.CollectOutputData = True
		  response = c.Perform
		  If response = 0 Then
		    Return True
		  Else
		    Return False
		  End
		  
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule

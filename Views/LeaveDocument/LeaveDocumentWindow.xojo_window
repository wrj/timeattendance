#tag Window
Begin Window LeaveDocumentWindow
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   800
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   414941183
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "ระบบการลา"
   Visible         =   True
   Width           =   1280
   Begin ctnMenuRibbon cRibbon
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFF00FFFF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   130
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   1280
   End
   Begin ctnLeaveContent cLeaveContent
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &c00FFFFFF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   632
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   314
      LockBottom      =   True
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   148
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   942
      Begin PushButton btnEdit
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "แก้ไข"
         Default         =   False
         Enabled         =   False
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "cLeaveContent"
         Italic          =   False
         Left            =   884
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   168
         Underline       =   False
         Visible         =   False
         Width           =   80
      End
      Begin PushButton btnNew
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "เพิ่ม"
         Default         =   False
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "cLeaveContent"
         Italic          =   False
         Left            =   799
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   168
         Underline       =   False
         Visible         =   True
         Width           =   80
      End
      Begin PushButton btnCancel
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "ยกเลิก"
         Default         =   False
         Enabled         =   False
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "cLeaveContent"
         Italic          =   False
         Left            =   973
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   168
         Underline       =   False
         Visible         =   False
         Width           =   80
      End
      Begin PushButton btnDelete
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "ลบ"
         Default         =   False
         Enabled         =   False
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "cLeaveContent"
         Italic          =   False
         Left            =   1065
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   168
         Underline       =   False
         Visible         =   False
         Width           =   80
      End
      Begin PushButton btnSave
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "บันทึก"
         Default         =   False
         Enabled         =   False
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "cLeaveContent"
         Italic          =   False
         Left            =   1156
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   6
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   168
         Underline       =   False
         Visible         =   False
         Width           =   80
      End
   End
   Begin PagePanel pgpLeave
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   582
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   314
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      PanelCount      =   2
      Panels          =   ""
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      Top             =   198
      Value           =   1
      Visible         =   True
      Width           =   942
      Begin ctnLeaveDocumentForm cLeaveDocumentForm
         AcceptFocus     =   False
         AcceptTabs      =   True
         AutoDeactivate  =   True
         BackColor       =   &cFF00FFFF
         Backdrop        =   0
         Enabled         =   True
         EraseBackground =   True
         HasBackColor    =   False
         Height          =   582
         HelpTag         =   ""
         InitialParent   =   "pgpLeave"
         Left            =   314
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   1
         TabStop         =   True
         Top             =   198
         Transparent     =   True
         UseFocusRing    =   False
         Visible         =   True
         Width           =   942
      End
      Begin ctnAddNewLeaveDocumentForm cAddNewLeaveDocumentForm
         AcceptFocus     =   False
         AcceptTabs      =   True
         AutoDeactivate  =   True
         BackColor       =   &cE9E9E900
         Backdrop        =   0
         Enabled         =   True
         EraseBackground =   True
         HasBackColor    =   False
         Height          =   582
         HelpTag         =   ""
         InitialParent   =   "pgpLeave"
         Left            =   314
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         numDayUpFile    =   0
         orgId           =   ""
         Scope           =   0
         selectCause     =   False
         TabIndex        =   0
         TabPanelIndex   =   2
         TabStop         =   True
         Top             =   198
         Transparent     =   True
         uploadFile      =   False
         UseFocusRing    =   False
         Visible         =   True
         Width           =   942
      End
   End
   Begin ctnLeaveDocumentTreeView cLeaveDocumentTreeView
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFF00FFFF
      Backdrop        =   0
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   632
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   148
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   300
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  If Utils.WebService.IsConnect Then
		    cLeaveDocumentTreeView.LoadOrgList
		    cLeaveDocumentTreeView.popOrg.ListIndex = 0
		  Else
		    MsgBox("ไม่สามารถเชื่อมต่อข้อมูลได้ กรุณาลองใหม่อีกครั้ง")
		  End If
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Navigate(pageIndex AS String)
		  Dim inD As Integer = Val(pageIndex)
		  If Utils.WebService.IsConnect Then
		    Self.pgpLeave.Value = inD
		  Else
		    MsgBox("ไม่สามารถเชื่อมต่อข้อมูลได้ กรุณาลองใหม่อีกครั้ง")
		  End If
		End Sub
	#tag EndMethod


#tag EndWindowCode

#tag Events btnEdit
	#tag Event
		Sub Action()
		  // gBtn = "editLeaveForm"
		  LeaveDocumentWindow.pgpLeave.Value = 1
		  Self.btnEdit.Visible = false
		  Self.btnEdit.Enabled = false
		  Self.btnCancel.Visible = true
		  Self.btnCancel.Enabled = true
		  Self.btnSave.Visible = true
		  Self.btnSave.Enabled = true
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnNew
	#tag Event
		Sub Action()
		  LeaveDocumentWindow.pgpLeave.Value = 1
		  cAddNewLeaveDocumentForm.NewDocument(cLeaveDocumentTreeView.popOrg.RowTag(cLeaveDocumentTreeView.popOrg.ListIndex))
		  Self.btnEdit.Visible = false
		  Self.btnEdit.Enabled = false
		  Self.btnDelete.Visible = false
		  Self.btnDelete.Enabled = false
		  Self.btnCancel.Visible = true
		  Self.btnCancel.Enabled = true
		  Self.btnSave.Visible = true
		  Self.btnSave.Enabled = true
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnCancel
	#tag Event
		Sub Action()
		  LeaveDocumentWindow.pgpLeave.Value = 0
		  btnCancel.Visible = false
		  btnCancel.Enabled = false
		  btnDelete.Visible = true
		  btnDelete.Enabled = true
		  btnEdit.Visible = true
		  btnEdit.Enabled = true
		  btnSave.Visible = false
		  btnSave.Enabled = false
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnDelete
	#tag Event
		Sub Action()
		  cAddNewLeaveDocumentForm.DeleteData
		  // gBtn = "editLeaveForm"
		  // ManagerLeaveSystemWindow.pgpLeave.Value = 1
		  // ManagerLeaveSystemWindow.cAddNewLeaveDocumentForm.SetID(Self.currentID)
		  // ManagerLeaveSystemWindow.cAddNewLeaveDocumentForm.lstLeaveType.Enabled  = False
		  // ManagerLeaveSystemWindow.cAddNewLeaveDocumentForm.btnSave.Caption = "แก้ไข"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSave
	#tag Event
		Sub Action()
		  cAddNewLeaveDocumentForm.BindData
		  btnSave.Visible = false
		  btnSave.Enabled = false
		  btnCancel.Visible = false
		  btnCancel.Enabled = false
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pgpLeave
	#tag Event
		Sub Change()
		  Dim p As Integer = me.Value
		  'MsgBox p.ToText
		  If p = 0 Then
		    cLeaveContent.lblTitle.Text = "ข้อมูลการลา"
		  Elseif p = 1 Then
		    If gBtn = "addLeaveForm" Then
		      cLeaveContent.lblTitle.Text = "เพิ่มใบลา"
		      
		    Elseif gBtn = "editLeaveForm" Then
		      cLeaveContent.lblTitle.Text = "แก้ไขใบลา"
		    End
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Group="OS X (Carbon)"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Frame"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Frame"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Group="OS X (Carbon)"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Menus"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Frame"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

#tag Window
Begin Window ImportLogWindow
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   183
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Import"
   Visible         =   True
   Width           =   267
   Begin CalendarControl CalendarControl1
      AutoDeactivate  =   True
      AutoLocalize    =   False
      BackColor       =   &cFFFFFF00
      ClickableOutOfBounds=   True
      DisabledSpecialDayColor=   &cC8727200
      DrawFrame       =   True
      DrawOutOfBounds =   False
      Enabled         =   True
      FirstDayOfWeek  =   0
      FridayEnabled   =   True
      FridaySpecial   =   False
      Height          =   143
      HelpTag         =   ""
      HideYearArrows  =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitMaxDay      =   0
      InitMaxMonth    =   0
      InitMaxYear     =   0
      InitMinDay      =   0
      InitMinMonth    =   0
      InitMinYear     =   0
      InitSelectedDay =   0
      InitSelectedMonth=   0
      InitSelectedYear=   0
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MarkStyle       =   0
      MondayEnabled   =   True
      MondaySpecial   =   False
      SaturdayEnabled =   True
      SaturdaySpecial =   False
      Scope           =   0
      SpecialDayColor =   &cC8000000
      SpecialWDayStyle=   0
      SundayEnabled   =   True
      SundaySpecial   =   False
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      ThursdayEnabled =   True
      ThursdaySpecial =   False
      Top             =   20
      TuesdayEnabled  =   True
      TuesdaySpecial  =   False
      UseBackColor    =   False
      UseSpecialDayColor=   False
      Visible         =   True
      WednesdayEnabled=   True
      WednesdaySpecial=   False
      Width           =   135
   End
   Begin PushButton PushButton1
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Import"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   167
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   20
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
#tag EndWindowCode

